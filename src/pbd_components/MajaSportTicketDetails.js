import React, { Component } from 'react'
import { Popup } from 'semantic-ui-react'
import axios from 'axios'
import api from '../api'

class MajaSportTicketDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      match: this.props.match,
      betId: this.props.betId,
      ticketInfos: this.props.ticketInfos,
    }
  }

  getMJSDetails = async bet_id => {
    const x = await axios.get(`${api.backendUrl}mjs/getTicket/${this.state.betId}`).then(result => {
      return result.data
    })
    this.setState({ [bet_id]: x })
  }

  render() {
    const { match, betId, ticketInfos } = this.state

    return (
      <div>
        <Popup
          trigger={
            <div>
              {match ? (
                <React.Fragment>
                  {match.league.name} <small>({match.sport_name})</small>
                  <br />
                  {match.h_team.name} vs {match.a_team ? match.a_team.name : ''} <br />
                </React.Fragment>
              ) : (
                'Error, Check with majasport'
              )}
            </div>
          }
          content={() => {
            const x = this.state[betId]
            if (x) {
              return (
                <React.Fragment>
                  Bet amount: {x.data.bet_amount} <br />
                  Odds Type: {x.data.odds_type} <br />
                  Handicap: {x.data.point} <br />
                  Odds: {x.data.odds} <br />
                  Bet: {x.data.side} <br />
                  Status : {x.data.is_void ? 'void' : x.data.win_state}
                </React.Fragment>
              )
            }
          }}
          on='click'
          open={this.state.isOpen}
          onClose={() => {
            this.setState({ isOpen: false, [betId]: '' })
          }}
          onOpen={() => {
            this.setState({ isOpen: true })
            this.getMJSDetails(this.state.betId)
          }}
          pinned
          wide
          position='top right'
          popperDependencies={[!!this.state[betId]]}
          style={{
            borderRadius: 0,
            opacity: 1,
            padding: '1em',
          }}
        />
      </div>
    )
  }
}

export default MajaSportTicketDetails
