import * as CryptoJS from 'crypto-js'
import * as _ from 'lodash'
import moment from 'moment-timezone'
import ApolloClient, { gql } from 'apollo-boost'
import { createApolloFetch } from 'apollo-fetch'
import jwt from 'jwt-decode'
import crypto from 'crypto'
import axios from 'axios'

class api {
  static backendUrl = process.env.REACT_APP_BACKEND_URL
  static walletUrl = process.env.REACT_APP_WALLET_URL

  static hostName = 'https://' + window.location.hostname
  static paymentUrl = process.env.REACT_APP_BACKEND_URL + 'paytrust/deposit/start'
  static Graph_uri = process.env.REACT_APP_GRAPHQL_URL
  static isAppMobile = /Mobi/.test(navigator.userAgent)
  static agentName = process.env.REACT_APP_MAIN_PARENT
  static milhouseURL = process.env.REACT_APP_MILHOUSE_URL
  static hababrandid = process.env.REACT_APP_HABANERO_BRANDID
  static sID = process.env.REACT_APP_TWILIO_SID

  static elyLaunchURL = process.env.REACT_APP_ELY_LAUNCH_URL || 'https://elysium.texas888.net'

  static client = new ApolloClient({
    uri: process.env.REACT_APP_GRAPHQL_URL,
  })

  static queryPlayersBet = `
    query PBS($playerName: String!, $dateStart: String!, $dateEnd: String!, $transactionStartDate: String!) {
      pbs(playerName: $playerName, dateStart: $dateStart, dateEnd: $dateEnd) {
        date
        validTurnover
        playerWinLoss
      }
      
      playerTransactions(
        first: 10
        dateStart: $transactionStartDate
        dateEnd: $dateEnd
        playerName: $playerName
        providerName: "ADMIN"
      ) {
        offset
        count
        playerLines {
          playerName
          transactionTime
          type
          winLose
          status
          providerName
        }
      }
      
    }
    
  `

  static PBDQuery = gql`
    query(
      $first: Int!
      $offset: Int!
      $playerName: String!
      $dateStart: String!
      $dateEnd: String!
      $agentName: String!
    ) {
      playerBetDetails(
        first: $first
        offset: $offset
        providerName: ""
        agentName: $agentName
        playerName: $playerName
        dateStart: $dateStart
        dateEnd: $dateEnd
      ) {
        offset
        count
        playerLines {
          playerName
          transactionTime
          transaction
          api_timestamp
          type
          stake
          winLose
          status
          providerName
          afterBalance
          freeGame
          bets
        }
      }
    }
  `

  static getCurrentUser() {
    // console.log(process.env.REACT_APP_BACKEND_URL)
    var res = sessionStorage.getItem('cronos')
    if (_.isEmpty(res)) {
      return null
    } else {
      return JSON.parse(atob(res))
    }
  }

  static setCurrentUser(userJson) {
    const x = btoa(JSON.stringify(userJson))
    return sessionStorage.setItem('cronos', x)
  }

  static getSitesInfo() {
    return JSON.parse(sessionStorage.getItem('dv-sitesInfo'))
  }

  static setSitesInfo(sites) {
    return sessionStorage.setItem('dv-sitesInfo', JSON.stringify(sites))
  }

  static logout() {
    const user = this.getCurrentUser()
    window._slaask.destroy()
    return Promise.all([api.kickIon(user), api.kickJDB(user)]).then(([x, y]) => {
      sessionStorage.clear()
    })
  }

  static getPaytrustUrl(userName, amount) {
    return fetch(
      `${this.paymentUrl}?amount=${amount}&name=${userName}&returnUrl=${this.hostName}/thank-you&failedUrl=${this.hostName}/profile`,
    )
  }

  static getCheckSum(str) {
    return btoa(CryptoJS.MD5.hashStr(str, true).toString())
  }

  static getTimestampFormatted() {
    const datenow = moment(new Date())
    const re = datenow.tz('America/Bogota').format()
    return re.toString().slice(0, -6)
  }

  static checkIfUsernameExist(playerName) {
    const uri = this.Graph_uri
    const query = `query attrByUsername ($playerName: String!) {
                checkuserByAttr(
                  playerName: $playerName
                )
                {
                  exist
                }
              }`

    const variables = {
      playerName: playerName,
    }
    const apolloFetch = createApolloFetch({ uri })
    return apolloFetch({ query, variables })
      .then(x => x)
      .catch(e => console.error(e))
    //return fetch(this.backendUrl + 'user/checkUser?userName=' + username.toLowerCase()).then((x) => x.json()).catch((e) => console.error(e));
  }

  static checkIfAgentNameExist(agentName) {
    return fetch(this.backendUrl + 'checkAgent?agentName=' + agentName.toLowerCase())
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static checkIfEMailExist(eMail) {
    // return fetch(this.backendUrl + 'users/search?email=' + eMail.toLowerCase())
    //   .then(x => x.json())
    //   .catch(e => console.error(e))
    const uri = this.Graph_uri
    const query = `query attrByUsername ($email: String!) {
                checkuserByAttr(
                  email: $email
                )
                {
                  exist
                }
              }`

    const variables = {
      email: eMail,
    }
    const apolloFetch = createApolloFetch({ uri })
    return apolloFetch({ query, variables })
      .then(x => x)
      .catch(e => console.error(e))
  }

  static getParentInfo(username) {
    return fetch(`${this.milhouseURL}/user/${username}`, {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
    })
  }

  static getChildrenList(username) {
    return fetch(`${this.milhouseURL}/users/parent/${username}`, {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
    })
  }

  static withdrawChildrenAmount(username, amount) {
    return fetch(`${this.milhouseURL}/user/${username}/withdraw`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
      body: JSON.stringify({
        amount,
      }),
    })
  }

  static checkIfTelephoneExist(telephone) {
    const uri = this.Graph_uri
    const query = `query attrByTelephone ($agentName: String!, $telephoneNo: String!) {
                checkuserByAttr(
                  agentName: $agentName,
                  telephoneNo: $telephoneNo
                )
                {
                  exist
                }
              }`

    const variables = {
      agentName: this.agentName,
      telephoneNo: telephone,
    }
    const apolloFetch = createApolloFetch({ uri })
    return apolloFetch({ query, variables })
      .then(x => x)
      .catch(e => console.error(e))
    //return fetch(this.backendUrl + 'users/search?telephone=' + encodeURIComponent(telephone)).then((x) => x.json()).catch((e) => console.error(e))
  }

  static checkifBankAccountExist(bankAccount) {
    const uri = this.Graph_uri
    const query = `query attrByBankAccount ($agentName: String!, $accountNo: String!) {
                checkuserByAttr(
                  agentName: $agentName,
                  accountNo: $accountNo
                )
                {
                  exist
                }
              }`

    const variables = {
      agentName: this.agentName,
      accountNo: bankAccount,
    }
    const apolloFetch = createApolloFetch({ uri })
    return apolloFetch({ query, variables })
      .then(x => x)
      .catch(e => console.error(e))

    //return fetch(this.backendUrl + 'users/search?bankaccount=' + bankAccount).then((x) => x.json()).catch((e) => console.error(e))
  }

  static registerUser(userJson) {
    const users = JSON.parse(userJson)
    const hash = crypto
      .createHash('md5')
      .update(`${users.userName}${users.password}${this.agentName}REGIS`)
      .digest('hex')

    this.updateFavList(users.userName)

    return fetch(this.backendUrl + 'user/v2/insert', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-Sig': hash,
      },
      body: userJson,
    })
  }

  static updateFavList(username) {
    axios.post(`${process.env.REACT_APP_FAVORITE_LIST}/getFavList`, null, {
      params: {
        user: username,
        list: [],
      },
    })
  }

  static changePassword(password) {
    return fetch(this.backendUrl + 'user/pwd', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
      body: JSON.stringify({
        password,
      }),
    })
  }

  static login(loginCredentials) {
    return fetch(this.backendUrl + 'user/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: loginCredentials,
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static getBalance() {
    return fetch(`${this.backendUrl}user/balance?r=${Math.random()}`, {
      headers: {
        'cache-control': 'no-cache',
        Authorization: 'Bearer ' + (this.getCurrentUser() ? this.getCurrentUser().token : null),
      },
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static toBase64 = file =>
    new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => resolve(reader.result)
      reader.onerror = error => reject(error)
    })

  static async depositMoney(username, amount, transferBy, file) {
    let image = false
    if (file) {
      image = await this.toBase64(file)
    }

    const hash = crypto
      .createHash('md5')
      .update(`${username}${amount}${this.agentName}`)
      .digest('hex')
    return fetch(
      this.walletUrl + `admin/user/deposit?r=${Math.random()}`,
      // `user/deposit?username=${username.toLowerCase()}&amount=${amount}&actionBy=${username.toLowerCase()}`,
      {
        headers: {
          Authorization: 'Bearer ' + this.getCurrentUser().token,
          'X-Sig': hash,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          amount,
          actionBy: username.toLowerCase(),
          pulsa: transferBy === 'pulsatsel' || transferBy === 'pulsaxl' ? true : false,
          ovoGoPay: transferBy === 'ovoGoPay' ? true : false,
          receipt: image,
        }),
        method: 'POST',
      },
    )
  }

  static withdrawMoney(username, amount) {
    return fetch(this.walletUrl + `admin/user/withdraw?r=${Math.random()}`, {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        amount,
      }),
      method: 'POST',
    })
  }

  static getBankAccount() {
    return fetch(this.backendUrl + 'user/bank', {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
    })
  }

  static getBankList() {
    return fetch(`${this.backendUrl}getBanksIDR/${this.agentName}?r=${Math.random()}`, {
      method: 'POST',
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static getUser(username) {
    return fetch(this.backendUrl + 'user/get?username=' + username.toLowerCase(), {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
    })
  }

  static kickJDB() {
    const dateNow = new Date()
    const json = {
      action: 17,
      ts: dateNow.getTime(),
      parent: 'cadyag',
    }
    return fetch(this.backendUrl + 'JDB', {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ x: json }),
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static kickIon() {
    return fetch(`${this.backendUrl}casino/LogOutPlayer`, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(x => x.json())
      .catch(e => console.error(e))
    //return Promise.resolve()
  }

  static kickSpade() {
    return fetch(`${this.backendUrl}spade/kick`, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(x => x.json())
      .catch(e => console.error(e))
    //return Promise.resolve()
  }

  static loginSBO() {
    const lang = localStorage.getItem('preferredLocale') === 'en' ? 'en' : 'th-TH'

    const appType = api.isAppMobile ? 2 : 1

    return fetch(this.backendUrl + 'sbobet/login', {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ lang, device: appType }),
    })
  }

  static getGamesList() {
    //return fetch(`SlotsGoldGamesList.json?noCache=${Math.random()}`)
    return fetch(`${this.backendUrl}getGames/${this.agentName}?r=${Math.random()}`).catch(e => console.error(e))
  }

  static enterDragoon(game_id) {
    // const lang = localStorage.getItem('preferredLocale') === 'en' ? 'en_us' : 'th_th'
    const lang = 'en_us'
    const theBody = {
      game_id,
      lang,
      backurl: `${window.location.href}`,
    }

    return fetch(`${this.backendUrl}dragoon/launch`, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(theBody),
    })
  }

  static enterMJS() {
    const theBody = {
      platform: this.isAppMobile ? 'mobile' : 'desktop',
    }
    return fetch(`${this.backendUrl}mjs/launch`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
      body: JSON.stringify(theBody),
    })
  }

  static enterELY() {
    const user = jwt(this.getCurrentUser().token)
    return `${this.elyLaunchURL}/?e=alpha&token=${user.yggToken}`
  }

  static getBannersList() {
    // return fetch('bannerData.json').then((x) => x.json()).catch((e) => console.error(e))
    return fetch(`${this.backendUrl}getBanners/${this.agentName}?r=${Math.random()}`, {
      method: 'POST',
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static getPromos() {
    return fetch(`${this.backendUrl}getARESfaqs/${this.agentName}?r=${Math.random()}`, {
      method: 'POST',
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static getUMStatusSBO() {
    return fetch(this.backendUrl + 'sbobet/status', {
      method: 'POST',
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static validatePhoneNumber(phoneNumber, channel) {
    const theBody = {
      locale: localStorage.getItem('preferredLocale') || 'th',
      phoneNumber,
      agentName: this.agentName,
      channel,
      sID: this.sID,
    }

    return fetch(`${this.backendUrl}sms/check`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(theBody),
    })
  }

  static verifyOTP(phoneNumber, code) {
    const theBody = {
      phoneNumber,
      code,
      sID: this.sID,
    }
    return fetch(`${this.backendUrl}sms/verify`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(theBody),
    })
  }

  static resetPassword(to) {
    const theBody = {
      to,
      agentName: this.agentName,
      hostName: this.hostName,
    }

    return fetch(`${this.backendUrl}user/resetPassword`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(theBody),
    })
  }
}

export default api
