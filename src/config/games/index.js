const USER_CATEGORY = 1
const PROMO_CATEGORY = 2
const NEW_CATEGORY_ID = 3
const SLOTS_CATEGORY_ID = 4
const TABLE_CATEGORY_ID = 5
const FISH_CATEGORY_ID = 6
const ARCADE_CATEGORY_ID = 7

const CATEGORY_CODES = [
  {
    id: USER_CATEGORY,
    icon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/fav.svg',
    activeIcon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/favB.svg',
    name: 'gameType.fav',
  },
  {
    id: PROMO_CATEGORY,
    icon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/promotion.svg',
    activeIcon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/promotionB.svg',
    name: 'gameType.promo',
  },
  {
    id: FISH_CATEGORY_ID,
    icon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/fishing.svg',
    activeIcon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/fishingB.svg',
    name: 'gameType.fishGames',
  },
  {
    id: NEW_CATEGORY_ID,
    icon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/favorite.svg',
    activeIcon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/favoriteB.svg',
    name: 'gameType.popular',
  },
  {
    id: SLOTS_CATEGORY_ID,
    icon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/slot.svg',
    activeIcon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/slotB.svg',
    name: 'gameType.slots',
  },
  {
    id: TABLE_CATEGORY_ID,
    icon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/table.svg',
    activeIcon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/tableB.svg',
    name: 'gameType.table',
  },
  {
    id: ARCADE_CATEGORY_ID,
    icon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/arcade.svg',
    activeIcon: 'https://ik.imagekit.io/xr5d8qn155ac5/qqd/newIcon/arcadeB.svg',
    name: 'gameType.arcade',
  },
]

export {
  CATEGORY_CODES as categoryCodes,
  PROMO_CATEGORY,
  NEW_CATEGORY_ID,
  SLOTS_CATEGORY_ID,
  TABLE_CATEGORY_ID,
  FISH_CATEGORY_ID,
  ARCADE_CATEGORY_ID,
  USER_CATEGORY,
  // LOTTERY_CATEGORY_ID,
}
