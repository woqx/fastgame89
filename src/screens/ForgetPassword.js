import React, { Component } from 'react'
import { Container, Card, Header, Input, Divider, Dimmer, Loader, Message } from 'semantic-ui-react'
import Helmet from 'react-helmet'
import { ErrContext } from '../contexts/Err'
import Translate, { translate } from '../locales/Translate'
import api from '../api'

class ForgetPassword extends Component {
  constructor() {
    super()
    this.state = {
      to: '',
      isLoading: false,
      isHidden: true,
      messageContent: '',
    }
  }

  handleMessage = e => {
    this.setState({ [e.target.name]: `+62${e.target.value.substring(1)}` })
  }

  render() {
    const { setErr, preferredLocale } = this.context
    const { buttonDisable, to, isLoading } = this.state
    return (
      <div>
        <Container>
          <Helmet>
            <title>{process.env.REACT_APP_MAIN_PARENT} | ลืมรหัสผ่าน</title>
            <meta name='description' content={`${process.env.REACT_APP_MAIN_PARENT} did you forget your pasword`} />
          </Helmet>
          <React.Fragment>
            <Dimmer.Dimmable blurring dimmed={this.state.isLoading}>
              <Card className='custom-form'>
                <Header as='h2' style={{ color: '#0b8040' }} className='form-header' textAlign='center'>
                  <Translate string='forgetpassword.resetHeader' />
                </Header>
                <Divider />
                <p></p>
                <Input
                  name='to'
                  disabled={buttonDisable}
                  placeholder={translate(preferredLocale, 'forgetpassword.placeholder')}
                  onChange={this.handleMessage}
                  type='number'
                  action={{
                    color: 'blue',
                    labelPosition: 'right',
                    icon: 'arrow right',
                    content: translate(preferredLocale, 'forgetpassword.changemypassword'),
                    onClick: async e => {
                      e.preventDefault()
                      if (!this.state.to) {
                        setErr(true, translate(preferredLocale, 'err.inputPhoneNumber'))
                      } else {
                        this.setState({ isLoading: true, buttonDisable: true })
                        api
                          .resetPassword(to)
                          .then(res => res.json())
                          .then(result => {
                            if (result.Code === 500) {
                              setErr(true, translate(preferredLocale, 'forgetpassword.invalidNumber'))
                              this.setState({ isLoading: false, buttonDisable: false })
                            } else {
                              this.setState({
                                isLoading: false,
                                isHidden: false,
                                messageContent: `${translate(preferredLocale, 'forgetpassword.resetSuccess')}`,
                              })
                            }
                          })
                      }
                    },
                  }}
                />
                <Message info hidden={this.state.isHidden} content={this.state.messageContent} />
              </Card>
              <Dimmer active={isLoading} inverted>
                <Loader active={isLoading} inline='centered' size='massive'>
                  <Translate string='register.pleasewaitchecking' />
                </Loader>
              </Dimmer>
            </Dimmer.Dimmable>
          </React.Fragment>
        </Container>
      </div>
    )
  }
}
ForgetPassword.contextType = ErrContext
export default ForgetPassword
