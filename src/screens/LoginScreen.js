import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import api from '../api'
import { Button, Container, Form, Message, Grid, Header, Card } from 'semantic-ui-react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import Translate from '../locales/Translate'
import Helmet from 'react-helmet'
import { UserContext } from '../contexts/User'

const LoginScreen = ({ siteInfos }) => {
  const [loginState, setLoginState] = useState({
    invalidCredentials: false,
    notAllowed: false,
    suspended: false,
    err_message: null,
  })

  const LoginSchema = Yup.object().shape({ userName: Yup.string().required(), password: Yup.string().required() })

  return (
    <Container>
      <UserContext.Consumer>
        {({ sitesInfo }) => {
          return (
            <Helmet>
              <title>{sitesInfo.login && `${sitesInfo.login.title}`}</title>
              <meta name='description' content={sitesInfo.login && `${sitesInfo.login.description}`} />
            </Helmet>
          )
        }}
      </UserContext.Consumer>

      <Card className='custom-form'>
        <Grid style={{ height: '100%' }} verticalAlign='middle'>
          <Grid.Column>
            <Header
              as='h2'
              style={{ color: '#0b8040', paddingBottom: '16px' }}
              className='form-header'
              textAlign='center'>
              <Translate string={'login.heading'} />
            </Header>
            <Formik
              initialValues={{
                userName: '',
              }}
              validationSchema={LoginSchema}
              onSubmit={(values, { setSubmitting }) => {
                api
                  .login(
                    JSON.stringify({
                      userName: values.userName.toLowerCase(),
                      password: values.password,
                    }),
                  )
                  .then(userInfo => {
                    if (userInfo.err === 500) {
                      setLoginState({ ...loginState, invalidCredentials: true, err_message: userInfo.err_message })
                      setSubmitting(false)
                      return
                    }
                    if (userInfo.agentName !== process.env.REACT_APP_MAIN_PARENT) {
                      setLoginState({ ...loginState, notAllowed: true })
                      setSubmitting(false)
                      return
                    }
                    if (userInfo.Result !== 'ACTIVE') {
                      setLoginState({ ...loginState, suspended: true })
                      setSubmitting(false)
                      return
                    }
                    api.setCurrentUser({
                      ...userInfo,
                      userName: values.userName.toLowerCase(),
                    })
                    window.location.href = '/'
                    setSubmitting(false)
                  })
                  .catch(e => {
                    setSubmitting(false)
                    console.warn(e)
                  })
              }}>
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                /* and other goodies */
              }) => (
                <Form loading={isSubmitting} onSubmit={handleSubmit} error={errors}>
                  <Form.Group widths='equal'>
                    <Form.Field
                      value={values.userName.toLowerCase()}
                      name='userName'
                      onChange={handleChange}
                      onBlur={handleBlur}
                      required
                      label={
                        <label>
                          <Translate string={'login.username'} />
                        </label>
                      }
                      control='input'
                    />
                  </Form.Group>
                  {touched.userName && errors.userName && <span className={'formError'}> {errors.userName} </span>}
                  <Form.Group widths='equal'>
                    <Form.Field
                      value={values.password}
                      name='password'
                      type='password'
                      onChange={handleChange}
                      onBlur={handleBlur}
                      required
                      label={
                        <label>
                          <Translate string={'login.password'} />
                        </label>
                      }
                      control='input'
                    />
                  </Form.Group>
                  {touched.password && errors.password && <span className={'formError'}> {errors.password} </span>}

                  {!loginState.invalidCredentials ? null : (
                    <Message
                      error
                      header={
                        <label>
                          <h3>
                            <Translate string='login.pleasetryagain' />
                          </h3>
                        </label>
                      }
                      content={
                        <label>
                          <Translate string='login.invalidCredential' />
                          <br />
                          {loginState.err_message}
                        </label>
                      }
                    />
                  )}
                  {!loginState.suspended ? null : (
                    <Message
                      error
                      header={
                        <label>
                          <h3>
                            <Translate string='login.suspendedHeader' />
                          </h3>
                        </label>
                      }
                      content={
                        <label>
                          <Translate string='login.suspendedContent' />
                        </label>
                      }
                    />
                  )}
                  {!loginState.notAllowed ? null : (
                    <Message
                      error
                      header={
                        <label>
                          <h3>
                            <Translate string='login.notAllowedHeader' />
                          </h3>
                        </label>
                      }
                      content={
                        <label>
                          <Translate string='login.notAllowedContent' />
                        </label>
                      }
                    />
                  )}
                  <Link to='/forgetPassword' style={{ color: 'black' }}>
                    <Translate string='login.forgetPassword' />
                  </Link>
                  <p></p>
                  <Button
                    type='submit'
                    onClick={handleSubmit}
                    style={{ backgroundColor: '#0b8040', color: 'white' }}
                    fluid
                    size='large'>
                    <Translate string={'submit'} />
                  </Button>
                </Form>
              )}
            </Formik>
          </Grid.Column>
        </Grid>
      </Card>
    </Container>
  )
}

export default LoginScreen
