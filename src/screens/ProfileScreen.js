import React, { useState, lazy, Suspense, useContext, useEffect, createContext } from 'react'
import Translate, { translate } from '../locales/Translate'
import { UserContext } from '../contexts/User'
import api from '../api'
import { Container, Card, Grid, Header, Message, Image, Tab, Segment, Divider, Button } from 'semantic-ui-react'

const AutoDepositForm = lazy(() => import('../components/AutoDepositForm'))
const DepositForm = lazy(() => import('../components/DepositForm'))
const WithdrawForm = lazy(() => import('../components/WithdrawForm'))

const renderLoader = () => (
  <div class='wrap'>
    <div class='loaderPacman kurt'>
      <p></p>
      <p></p>
      We are loading some fun contents for you to views!
    </div>
  </div>
)

const PromoFcRender = ctx => (
  <Container className='containerExpand' id={'profile-component'}>
    <Card className='custom-form'>
      <Grid style={{ height: '100%' }} verticalAlign='middle'>
        <Grid.Column>
          <Header
            as='h2'
            style={{ color: '#0b8040', paddingBottom: '16px' }}
            className='form-header'
            textAlign='center'>
            Transaction
          </Header>
          <Message header='Promo/Free - Mode ON' content={translate(ctx.preferredLocale, 'profile.fcON')} />
        </Grid.Column>
      </Grid>
    </Card>
  </Container>
)

const ValidateRender = ctx => (
  <Segment>
    <div style={{ color: 'black' }}>
      <Translate string='acc.validate' />
      <p></p>
    </div>
  </Segment>
)

const ProfileScreen = () => {
  const ctx = useContext(UserContext)
  const [ProfileState, setProfileState] = useState({
    banks: {},
    userBankAccounts: {},
    autoBankAccount: {},
    isValidate: false,
  })
  const [banks, setBanks] = useState({})
  const [autoBankSteps, setAutoBankSteps] = useState({})

  const getUserBankAccount = async () => {
    const result = await api
      .getBankAccount(ctx.user.userName)
      .then(json => json.json())
      .catch(e => console.error(e))
    // console.log(result)
    setProfileState({
      ...ProfileState,
      userBankAccounts: result,
      autoBankAccount: result.bankAccounts[0],
      isValidate: result.isValidate,
    })
  }

  const getOperatorBanks = async () => {
    const result = await api.getBankList().catch(e => console.error(e))
    // console.log(result.banks)
    setBanks(result.banks)
  }

  const fetchAutoBankHowTos = async () => {
    fetch('bankHowTo.json').then(async x => {
      const { steps } = await x.json()
      setAutoBankSteps(steps)
    })
  }

  useEffect(() => {
    getUserBankAccount()
    getOperatorBanks()
    fetchAutoBankHowTos()
  }, [])
  if (ctx.user.isPromo || ctx.user.isFC) {
    return PromoFcRender(ctx)
  }

  const panes = [
    {
      menuItem: {
        content: (
          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
            <Image
              size='mini'
              src={`${process.env.REACT_APP_IMAGE_URL}/siam44/deposit.svg`}
              style={{ margin: 'auto', height: '20px', width: '20px', marginRight: '8px' }}
            />
            <div style={{ margin: 'auto' }}>
              <Translate string={'profile.deposit'} />
            </div>
          </div>
        ),
      },
      render: () => {
        if (!ProfileState.autoBankAccount.isShow) {
          return (
            <Tab.Pane>
              {ProfileState.isValidate ? (
                <DepositForm banks={banks} userBank={ProfileState.userBankAccounts} userName={ctx.user.userName} />
              ) : (
                ValidateRender(ctx)
              )}
            </Tab.Pane>
          )
        }
        return (
          <Tab.Pane>
            {ProfileState.isValidate ? (
              <AutoDepositForm bankAccount={ProfileState.autoBankAccount} howTos={autoBankSteps} />
            ) : (
              ValidateRender(ctx)
            )}
          </Tab.Pane>
        )
      },
    },
    {
      menuItem: {
        content: (
          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
            <Image
              size='mini'
              src={`${process.env.REACT_APP_IMAGE_URL}/siam44/withdraw.svg`}
              style={{ margin: 'auto', height: '20px', width: '20px', marginRight: '8px' }}
            />
            <div style={{ margin: 'auto' }}>
              <Translate string={'profile.withdraw'} />
            </div>
          </div>
        ),
      },
      render: () => (
        <Tab.Pane>
          <WithdrawForm userBank={ProfileState.userBankAccounts} />
        </Tab.Pane>
      ),
    },
  ]

  return (
    <Container className='containerExpand' id={'profile-component'}>
      <Card className='custom-form'>
        <Grid style={{ height: '100%' }} verticalAlign='middle'>
          <Grid.Column>
            <Header
              as='h2'
              style={{ color: '#0b8040', paddingBottom: '16px' }}
              className='form-header'
              textAlign='center'>
              Transaction
            </Header>
            <Tab panes={panes} />
          </Grid.Column>
        </Grid>
      </Card>
    </Container>
  )
}

export default ProfileScreen
