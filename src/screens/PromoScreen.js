import React from 'react'
import api from '../api'
import { Helmet } from 'react-helmet'
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton,
} from 'react-accessible-accordion'
// Demo styles, see 'Styles' section below for some notes on use.
import 'react-accessible-accordion/dist/fancy-example.css'
import { Container } from 'semantic-ui-react'
import Translate from '../locales/Translate'
import { UserContext } from '../contexts/User'

export default class PromoScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      promos: [],
    }
  }

  async componentDidMount() {
    const promos = await api.getPromos()

    this.setState({ promos })
  }

  render() {
    const { promos } = this.state
    return (
      <Container className='containerExpand'>
        <Helmet>
          <title>Promo | FASTGAMES89</title>
          <meta
            name='description'
            content='Nikmati promo bonus game berupa cashback dan bonus new member slot, live casino dan arcade terbesar di Indonesia.'
          />
        </Helmet>
        <p></p>
        <h2>
          <Translate string='promo.header' />
        </h2>
        <Accordion allowMultipleExpanded={true} allowZeroExpanded={true}>
          {promos.map(x => {
            return (
              <AccordionItem>
                <AccordionItemHeading>
                  <AccordionItemButton>{x[`Title${this.context.preferredLocale}`]}</AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel style={{ backgroundColor: '#d6d8d9', color: '#1b1e21' }}>
                  <div dangerouslySetInnerHTML={{ __html: x[`Content${this.context.preferredLocale}`] }} />
                </AccordionItemPanel>
              </AccordionItem>
            )
          })}
        </Accordion>
      </Container>
    )
  }
}
PromoScreen.contextType = UserContext
