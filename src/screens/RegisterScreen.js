import React, { Fragment } from 'react'
import api from '../api'
import {
  Button,
  Container,
  Form,
  Message,
  FormGroup,
  FormField,
  Grid,
  Card,
  Header,
  Modal,
  Icon,
  Dimmer,
  Loader,
  List,
  Input,
  Divider,
} from 'semantic-ui-react'
import { Formik, Field } from 'formik'
import * as Yup from 'yup'
import * as _ from 'lodash'
import Translate, { translate } from '../locales/Translate'
import { LocaleContext } from '../contexts/Locale'
import { Helmet } from 'react-helmet'
import axios from 'axios'
import ReCAPTCHA from 'react-google-recaptcha'

const AsyncUserNameField = ({ field, form: { errors, touched }, ...props }) => {
  return (
    <Fragment>
      <Form.Field
        type='text'
        {...field}
        {...props}
        style={
          (_.has(errors, 'userName') && _.hasIn(touched, 'userName')) || field.value === ''
            ? {}
            : { borderColor: 'green' }
        }
      />
    </Fragment>
  )
}

const AsyncAgentNameField = ({ field, form: { errors, touched }, ...props }) => {
  return (
    <Fragment>
      <Form.Field
        type='text'
        {...field}
        {...props}
        style={
          (_.has(errors, 'agentName') && _.hasIn(touched, 'agentName')) || field.value === ''
            ? {}
            : { borderColor: 'green' }
        }
      />
    </Fragment>
  )
}

const AsyncEmailField = ({ field, form: { errors, touched }, ...props }) => {
  return (
    <Fragment>
      <Form.Field
        type='text'
        {...field}
        {...props}
        style={
          (_.has(errors, 'eMail') && _.hasIn(touched, 'eMail')) || field.value === '' ? {} : { borderColor: 'green' }
        }
      />
    </Fragment>
  )
}

const AsyncTelephoneField = ({ field, form: { errors, touched }, ...props }) => {
  return (
    <Fragment>
      <Form.Field
        type='text'
        {...field}
        {...props}
        style={
          (_.has(errors, 'telephone') && _.hasIn(touched, 'telephone')) || field.value === ''
            ? {}
            : { borderColor: 'green' }
        }
      />
    </Fragment>
  )
}

const AsyncBankAccountField = ({ field, form: { errors, touched }, ...props }) => {
  return (
    <Fragment>
      <Form.Field
        type='text'
        {...field}
        {...props}
        style={
          (_.has(errors, 'bankAccount') && _.hasIn(touched, 'bankAccount')) || field.value === ''
            ? {}
            : { borderColor: 'green' }
        }
      />
    </Fragment>
  )
}

class RegisterScreen extends React.Component {
  constructor(props) {
    super(props)
    const urlSearchParams = new URLSearchParams(this.props.location.search)
    this.state = {
      defaultBankName: urlSearchParams.get('bankName'),
      defaultAgentName: urlSearchParams.get('agentName'),
      referralParent: urlSearchParams.get('ref'),
      countryName: '',
      countryCode: '',
      showModal: false,
      MessagePopUp: '',
      userName: '',
      password: '',
      telephone: localStorage.getItem('telephone'),
      isValidate: true,
    }
  }

  componentDidMount() {
    const urlSearchParams = new URLSearchParams(this.props.location.search)
    if (urlSearchParams.get('ref')) {
      sessionStorage.setItem('ref', urlSearchParams.get('ref'))
      this.forceUpdate()
    }
  }

  RegisterSchema = Yup.object().shape({
    password: Yup.string()
      .required('PIN is required')
      // .matches(/(?=^.{4,10}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$/,
      .matches(/^\d{6}$/, {
        excludeEmptyString: true,
        message: 'Password 6 digits numerical only',
      }),
    telephone: Yup.string()
      .required('Telephone is required')
      .matches(/^\d{9,}$/, {
        message: 'Nomor HP anda!',
        excludeEmptyString: true,
      })
      .test('moreThan', 'Telephone Number must not be more than 15 characters', val => {
        if (_.isEmpty(val)) return null
        return val.toString().length < 15
      }),
    bankAccount: Yup.string()
      .required('Bank Account is required')
      .matches(/\d{1,15}$/, {
        message: 'Must be all numbers',
        excludeEmptyString: true,
      })
      .test('moreThan', 'Account Number must be more than 8 characters', val => {
        if (_.isEmpty(val)) return null
        return val.toString().length > 8
      })
      .test('moreThan', 'Account Number must not be more than 18 characters', val => {
        if (_.isEmpty(val)) return null
        return val.toString().length < 19
      }),
    // eMail: Yup.string()
    //   .required('Email is required')
    //   .email('Invalid email'),
    userName: Yup.string()
      .required('Username is required')
      .matches(/^[a-z0-9_-]*$/, {
        message: 'Username only a-z,0-9, and - or _',
        excludeEmptyString: true,
      }),
    firstName: Yup.string().required('LINE/WECHAT/WHATSAPP จำเป็น'),
    lastName: Yup.string().required('Last Name is required'),
    currency: Yup.string().required('Currency is required'),
    bank: Yup.string().required('Bank is required'),
    bankName: Yup.string().required('Bank Name is required'),
    agentName: Yup.string().required('Agent Name is required'),
    captchaResponse: Yup.string().required('Please check this, is required'),
  })

  async validateUserName(username) {
    if (_.isEmpty(username)) {
      return null
    }
    return api.checkIfUsernameExist(username).then(x => {
      if (JSON.stringify(x.data.checkuserByAttr.exist) === 'true') {
        return 'Username ini sudah terdaftar, silahkan pilih username lain'
      }
      // if (Object.keys(x).length !== 0) {
      //     return "Username is already taken"
      // }
    })
  }

  async validateAgentName(agentName) {
    if (_.isEmpty(agentName)) {
      return null
    }
    return api.checkIfAgentNameExist(agentName).then(x => {
      if (Object.keys(x).length === 0) {
        return 'Invalid Agent Name'
      }
    })
  }

  async validateEmail(eMail) {
    if (_.isEmpty(eMail)) {
      return null
    }
    return api.checkIfEMailExist(eMail).then(x => {
      if (JSON.stringify(x.data.checkuserByAttr.exist) === 'true') {
        return 'Email ini telah terdaftar'
      }
      // if (Object.keys(x).length !== 0) {
      //     return "ที่อยู่อีเมลนี้ได้รับการลงทะเบียนแล้ว"
      //   }
    })
  }

  async validateTelephone(Telephone) {
    if (_.isEmpty(Telephone)) {
      return null
    }

    const telephone = `+62${Telephone.substring(1)}`

    return api.checkIfTelephoneExist(telephone).then(x => {
      if (JSON.stringify(x.data.checkuserByAttr.exist) === 'true') {
        return 'Nomor telepon ini telah terdaftar'
      }
      // if (Object.keys(x).length !== 0) {
      //     return "หมายเลขโทรศัพท์นี้ได้รับการลงทะเบียนแล้ว"
      //   }
    })
  }

  async validateBankAccount(bankAccount) {
    if (_.isEmpty(bankAccount)) {
      return null
    }
    if (bankAccount.includes(' ')) {
      return 'Jangan ada spasi'
    }
    if (bankAccount.includes('-')) {
      return "Hilangkan '-' "
    }
    return api.checkifBankAccountExist(bankAccount).then(x => {
      if (JSON.stringify(x.data.checkuserByAttr.exist) === 'true') {
        return 'Nomor Rekening Ini telah terdaftar! silahkan login'
      }
    })
  }

  handleClosePopUp = () => {
    api
      .login(
        JSON.stringify({
          userName: this.state.userName,
          password: this.state.password,
        }),
      )
      .then(userInfo => {
        api.setCurrentUser({
          ...userInfo,
          userName: this.state.userName,
        })
        this.setState({ showModal: false, MessagePopUp: '' })
        window.location.href = '/'
      })
      .catch(e => {
        this.setState({ loading: false })
        console.error(e)
        //setSubmitting(false);
      })
  }

  handleMessage = e => {
    if (e.target.name === 'telephone') {
      this.setState({ [e.target.name]: `+62${e.target.value.substring(1)}` })
    } else {
      this.setState({ [e.target.name]: e.target.value })
    }
  }

  render() {
    const { preferredLocale, setErr } = this.context
    const { isValidate, telephone, verify } = this.state

    return (
      <Container>
        <Helmet>
          <title>Daftar | FASTGAMES90</title>
          <meta
            name='description'
            content='Daftar bersama agen game online terbaik Indonesia FASTGAMES89 untuk merasakan sensasi bermain slot online paling fantastis.'
          />
        </Helmet>
        {isValidate ? (
          <Card className='custom-form'>
            <Grid style={{ height: '100%' }} verticalAlign='middle'>
              <Grid.Column>
                <Header
                  as='h2'
                  style={{ color: '#0b8040', paddingBottom: '16px' }}
                  className='form-header'
                  textAlign='center'>
                  <Translate string='register.heading' />
                </Header>
                <Formik
                  initialValues={{
                    bankName: this.state.defaultBankName || '',
                    agentName: this.state.defaultAgentName || process.env.REACT_APP_MAIN_PARENT,
                    referral: sessionStorage.getItem('ref') || this.state.referralParent || '',
                    currency: `${process.env.REACT_APP_CURRENCY}`,
                    eMail: 'null@null.com',
                    firstName: `fastgames89@${new Date().getTime()}`,
                    lastName: '--',
                    password: '',
                    telephone: this.state.telephone,
                    userName: '',
                    bankAccount: '',
                    bank: '',
                    sbo_minBet: 1,
                    sbo_maxBet: 100,
                    sbo_maxPerMatch: 1000,
                    tableLimit: 'LOW',
                    status: 'ACTIVE',
                    captchaResponse: this.state.captchaResponse,
                  }}
                  validationSchema={this.RegisterSchema}
                  onSubmit={(values, { setSubmitting }) => {
                    values.telephone = `+62${values.telephone.substring(1)}`

                    api
                      .registerUser(JSON.stringify(values))
                      .then(x => x.json())
                      .then(x => {
                        if (x.err === 500) {
                          alert(x.err_message)
                          setSubmitting(false)
                          return
                        }
                        this.setState({
                          showModal: true,
                          MessagePopUp: `Selamat anda telah terdaftar! Klik tombol di bawah untuk lanjut bermain`,
                          userName: values.userName,
                          password: values.password,
                        })
                      })
                      .catch(e => {
                        console.warn(e)
                        this.setState({ loading: false })
                      })
                  }}>
                  {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    setFieldValue,
                    /* and other goodies */
                  }) => (
                    <Form loading={isSubmitting} onSubmit={handleSubmit} error={errors}>
                      <Form.Group widths='equal'>
                        {sessionStorage.getItem('ref') || this.state.referralParent ? (
                          <Form.Field
                            control='input'
                            disabled
                            value={values.referral.toLowerCase()}
                            name='referral'
                            label='Referral'
                          />
                        ) : (
                          ''
                        )}
                      </Form.Group>
                      {/* <Form.Group widths='equal'>
                        <Field
                          value={values.eMail.toLowerCase()}
                          component={AsyncEmailField}
                          name='eMail'
                          validate={this.validateEmail}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          required
                          label={
                            <label>
                              <Translate string='register.email' />
                            </label>
                          }
                          control='input'
                          error={_.has(errors, 'eMail') && _.has(touched, 'eMail')}
                        />
                      </Form.Group>
                      {touched.eMail && errors.eMail && <span className={'formError'}> {errors.eMail} </span>} */}
                      <Form.Group widths='equal'>
                        <Field
                          value={values.userName.toLowerCase()}
                          component={AsyncUserNameField}
                          name='userName'
                          validate={this.validateUserName}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          required
                          label={
                            <label>
                              <Translate string='register.userName' />
                            </label>
                          }
                          control='input'
                          error={_.has(errors, 'userName') && _.has(touched, 'userName')}
                        />
                      </Form.Group>
                      {touched.userName && errors.userName && <span className={'formError'}> {errors.userName} </span>}
                      <Form.Group widths='equal'>
                        <Form.Field
                          type='password'
                          value={values.password}
                          pattern='[0-9]*'
                          inputmode='numeric'
                          placeholder={'eg: 201912'}
                          name='password'
                          onChange={handleChange}
                          onBlur={handleBlur}
                          required
                          label={
                            <label>
                              <Translate string='register.password' />
                            </label>
                          }
                          control='input'
                          error={_.has(errors, 'password') && _.has(touched, 'password')}
                        />
                      </Form.Group>
                      {touched.password && errors.password && <span className={'formError'}> {errors.password} </span>}
                      <Form.Group widths='equal'>
                        <Field
                          // readOnly
                          value={values.telephone}
                          component={AsyncTelephoneField}
                          name='telephone'
                          validate={this.validateTelephone}
                          onChange={handleChange}
                          placeholder={'082377665544'}
                          onBlur={handleBlur}
                          required
                          label={
                            <label>
                              <Translate string='register.telephone' />
                            </label>
                          }
                          control='input'
                          error={_.has(errors, 'telephone') && _.has(touched, 'telephone')}
                        />
                      </Form.Group>
                      {touched.telephone && errors.telephone && (
                        <span className={'formError'}> {errors.telephone} </span>
                      )}
                      <Form.Group widths='equal'>
                        <Form.Field
                          value={values.bankName}
                          name='bankName'
                          onChange={handleChange}
                          onBlur={handleBlur}
                          required
                          label={
                            <label>
                              <Translate string='register.bankName' />
                            </label>
                          }
                          control='input'
                          error={_.has(errors, 'bankName') && _.has(touched, 'bankName')}
                        />
                        <Field
                          value={values.bankAccount}
                          component={AsyncBankAccountField}
                          name='bankAccount'
                          validate={this.validateBankAccount}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          required
                          label={
                            <label>
                              <Translate string='register.bankAccount' />
                            </label>
                          }
                          control='input'
                          error={_.has(errors, 'bankAccount') && _.has(touched, 'bankAccount')}
                        />
                      </Form.Group>
                      {touched.bankName && errors.bankName && <span className={'formError'}> {errors.bankName} </span>}
                      {touched.bankAccount && errors.bankAccount && (
                        <span className={'formError'}> {errors.bankAccount} </span>
                      )}
                      <Form.Group widths='equal'>
                        <Form.Field
                          value={values.bank}
                          name='bank'
                          onChange={handleChange}
                          onBlur={handleBlur}
                          required
                          control='select'
                          label={
                            <label>
                              <Translate string='register.bank' />
                            </label>
                          }
                          error={_.has(errors, 'bank') && _.has(touched, 'bank')}>
                          <option value=''>PILIH BANK </option>
                          <option value='BCA'>{translate(preferredLocale, 'bank.bca')}</option>
                          <option value='MANDIRI'>{translate(preferredLocale, 'bank.mandiri')}</option>
                          <option value='BNI'>{translate(preferredLocale, 'bank.bni')}</option>
                          <option value='BRI'>{translate(preferredLocale, 'bank.bri')}</option>
                          <option value='CIMB'>{translate(preferredLocale, 'bank.cimb')}</option>
                          <option value='PERMATA'>{translate(preferredLocale, 'bank.permata')}</option>
                          <option value='PANIN'>{translate(preferredLocale, 'bank.panin')}</option>
                          {/* <option value='OVO'>{translate(preferredLocale, 'bank.ovo')}</option>
                          <option value='GOPAY'>{translate(preferredLocale, 'bank.gopay')}</option> */}
                          <option value='DANA'>{translate(preferredLocale, 'bank.dana')}</option>
                        </Form.Field>
                      </Form.Group>
                      {touched.bank && errors.bank && <span className={'formError'}> {errors.bank} </span>}
                      <ReCAPTCHA
                        name='captchaResponse'
                        sitekey='6LclLe8UAAAAAN5u2kMjF35WcbmmzJdpGV4c1F6H'
                        // onChange={this.storeCaptcha}
                        onChange={async e => {
                          setFieldValue('captchaResponse', e)
                        }}
                      />
                      <br />
                      {touched.captchaResponse && errors.captchaResponse && (
                        <span className={'formError'}> {errors.captchaResponse} </span>
                      )}

                      <Button
                        type='submit'
                        onClick={handleSubmit}
                        style={{ backgroundColor: '#0b8040', color: 'white' }}
                        fluid
                        size='large'>
                        <Translate string='submit' />
                      </Button>
                    </Form>
                  )}
                </Formik>
              </Grid.Column>
            </Grid>
          </Card>
        ) : (
          <React.Fragment>
            <Dimmer.Dimmable blurring dimmed={this.state.isLoading}>
              <Card className='custom-form'>
                <Message warning>
                  <Header>
                    <Icon name='bullhorn' />
                    <Translate string='register.validateHeader' />
                  </Header>

                  <List bulleted>
                    {this.state.steps &&
                      this.state.steps.map(step => (
                        <List.Item>
                          <Translate string={step} />
                        </List.Item>
                      ))}
                  </List>
                </Message>

                <Input
                  name='telephone'
                  disabled={this.state.buttonDisable}
                  placeholder='Masukan nomor telpon anda'
                  onChange={this.handleMessage}
                  type='number'
                  action={{
                    color: 'red',
                    labelPosition: 'right',
                    icon: 'text telephone',
                    content: 'Kirim OTP',
                    onClick: async e => {
                      e.preventDefault()
                      if (!this.state.telephone) {
                        setErr(true, translate(preferredLocale, 'err.inputPhoneNumber'))
                      } else {
                        this.setState({ isLoading: true })
                        api
                          .validatePhoneNumber(telephone, 'sms')
                          .then(res => res.json())
                          .then(x => {
                            if (x.Code === 500) {
                              setErr(true, translate(preferredLocale, 'err.PhoneRegistered'))
                              this.setState({ isLoading: false })
                            } else {
                              this.setState({ isLoading: false, buttonDisable: true })
                            }
                          })
                      }
                    },
                  }}
                />
                {this.state.buttonDisable ? (
                  <React.Fragment>
                    <Divider />
                    <Input
                      name='verify'
                      placeholder='Nomor OTP'
                      onChange={this.handleMessage}
                      type='number'
                      action={{
                        color: 'blue',
                        labelPosition: 'right',
                        icon: 'arrow right',
                        content: 'Validasi',
                        onClick: async e => {
                          e.preventDefault()
                          if (!this.state.verify) {
                            setErr(true, translate(preferredLocale, 'err.OTPnoEmpty'))
                          } else {
                            this.setState({ isLoading: true })
                            api
                              .verifyOTP(telephone, verify)
                              .then(res => res.json())
                              .then(x => {
                                console.log(x)
                                if (x.status === 'approved') {
                                  this.setState({ isLoading: false, isValidate: true })
                                } else {
                                  this.setState({ isLoading: false }, () => {
                                    setErr(true, translate(preferredLocale, 'err.OTPWrong'))
                                  })
                                }
                              })
                          }
                        },
                      }}
                    />
                    <a
                      style={{ color: '#000' }}
                      href='#'
                      onClick={e => {
                        e.preventDefault()
                        api.validatePhoneNumber(telephone, 'call')
                      }}>
                      <Translate string='register.callme' />
                    </a>
                  </React.Fragment>
                ) : (
                  ''
                )}
              </Card>
              <Dimmer active={this.state.isLoading} inverted>
                <Loader active={this.state.isLoading} inline='centered' size='massive'>
                  <Translate string='register.pleasewaitchecking' />
                </Loader>
              </Dimmer>
            </Dimmer.Dimmable>
          </React.Fragment>
        )}
        <Modal
          open={this.state.showModal}
          onClose={this.handleClosePopUp}
          header='Selamat anda telah terdaftar!'
          basic
          size='tiny'
          dimmer='blurring'>
          <Header icon='bullhorn' content='Selamat' />
          <Modal.Content>
            <Modal.Description>
              <h2>{this.state.MessagePopUp}</h2>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button color='green' onClick={this.handleClosePopUp} inverted>
              <Icon name='checkmark' /> Ok
            </Button>
          </Modal.Actions>
        </Modal>
      </Container>
    )
  }
}

RegisterScreen.contextType = LocaleContext
export default RegisterScreen
