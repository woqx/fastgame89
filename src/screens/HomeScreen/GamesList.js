import React, { useState, useEffect, useRef, useContext, createRef } from 'react'
import _ from 'lodash'
import { Button, Icon, Image } from 'semantic-ui-react'
import '../../assets/GamesList.css'
import iconRecommend from '../../assets/images/recommended.svg'
import iconPopular from '../../assets/images/popular.svg'
import iconHighlight from '../../assets/images/highlight.svg'
import Translate from '../../locales/Translate'
import api from '../../api'
import { UserContext } from '../../contexts/User'
import { pName } from '../../utils'

const Cats = ['isNew', 'isRecommend', 'isPOM']
const Highlights = window.proHigh[process.env.REACT_APP_MAIN_PARENT]

const GamesList = ({ activeGameCategory, allGames, G }) => {
  const [Games, setAllGames] = useState([])
  const [allG, setG] = useState([])
  const [shuffleGame, setShuffleGames] = useState([])
  const ctx = useContext(UserContext)
  const xSliderisNew = useRef(null)
  const xSliderisRecommend = useRef(null)
  const sliders = { xSliderisNew, xSliderisRecommend }
  const sliderHLRefs = useRef(Highlights.map((_, i) => React.createRef()))

  useEffect(() => {
    const shuffle = _.filter(_.shuffle(allGames), x => {
      return x.isRecommend
    })
    setAllGames(allGames)
    setG(G)
    setShuffleGames(shuffle)
  }, [allGames])

  const imageUrl = image => {
    if (_.isUndefined(image)) {
      return 'NULL'
    }
    if (image.indexOf('https') === 0) {
      return image
    }
    return `${process.env.REACT_APP_IMAGE_URL}${image}`
  }

  const scrollMe = (ref, direction) => {
    const slider = sliders[ref]
    const distance = (slider.current.clientWidth / 2) * direction
    slider.current.scrollLeft += distance
  }

  const scrollHighlight = (ref, direction) => {
    const distance = (ref.current.clientWidth / 2) * direction
    ref.current.scrollLeft += distance
  }

  return (
    <div
      style={
        activeGameCategory !== 3 && activeGameCategory !== 4
          ? { paddingTop: '0', paddingBottom: '0' }
          : { paddingTop: '20px', paddingBottom: '20px' }
      }>
      {(() => {
        if (activeGameCategory !== 3 && activeGameCategory !== 4) return false

        return (
          <React.Fragment>
            {Cats.map(c => {
              return (
                <React.Fragment>
                  <header className='GridHeaderComponent'>
                    <div className='GridHeaderComponent-left'>
                      <span className='GridHeaderComponent-icon'>
                        <Image
                          style={{ width: '38px !important' }}
                          src={(() => {
                            if (c === 'isNew') {
                              return iconPopular
                            }
                            if (c === 'isRecommend') {
                              return iconRecommend
                            }
                            if (c === 'isPOM') {
                              return iconHighlight
                            }
                          })()}
                        />
                      </span>
                      <div className='GridHeaderComponent-group'>
                        <div className='GridHeaderComponent-title'>
                          <Translate string={`home.${c}`} />
                        </div>
                      </div>
                    </div>
                    {api.isAppMobile ? (
                      ''
                    ) : c === 'isPOM' ? (
                      ''
                    ) : (
                      <Button.Group className='slider-button' basic inverted style={{ border: '1px solid #616168' }}>
                        <Button
                          style={{ color: 'white' }}
                          onClick={e => {
                            e.preventDefault()
                            scrollMe(`xSlider${c}`, -1)
                          }}>
                          <Icon name='arrow left' style={{ color: 'white' }} />
                        </Button>
                        <Button
                          onClick={e => {
                            e.preventDefault()
                            scrollMe(`xSlider${c}`, 1)
                          }}>
                          <Icon name='arrow right' style={{ color: 'white' }} />
                        </Button>
                      </Button.Group>
                    )}
                  </header>
                  <div className='app2' style={{ paddingTop: 0 }}>
                    {(() => {
                      let newGame = false
                      newGame = _.filter(Games, x => {
                        return x[c]
                      })

                      if (c === 'isNew') {
                        return (
                          <ul ref={xSliderisNew} className='hs hs1 full no-scrollbar'>
                            {newGame.map(y => {
                              const poms = process.env.REACT_APP_HIGHLIGHT_PROVIDER.split(',')
                              for (var i = 0; i < poms.length; i++) {
                                if (y.provider === poms[i]) return false
                              }

                              return (
                                <li className='itemCarousel'>
                                  <img
                                    class='test'
                                    style={{ width: '100%', height: '100%' }}
                                    onClick={() => y.onClick(y, ctx.user, ctx)}
                                    src={`${imageUrl(y.poster)}`}
                                    alt={y.name}
                                  />
                                </li>
                              )
                            })}
                          </ul>
                        )
                      }
                      if (c === 'isPOM') {
                        return (
                          <React.Fragment>
                            {Highlights.map((pom, idx) => {
                              const provGames = _.filter(allG, ['provider', `${pom}`])

                              return (
                                <React.Fragment>
                                  <div class='ui divider line-pos' style={{ position: 'relative' }}></div>
                                  <div class='logo-providers' style={{ display: 'inline-block' }}>
                                    <Image
                                      style={{ marginLeft: '0', marginTop: '0' }}
                                      src={`https://ik.imagekit.io/xr5d8qn155ac5/Provider-logo/v3/${pom}.png`}
                                    />
                                  </div>
                                  <ul ref={sliderHLRefs.current[idx]} className='hs hs2 full no-scrollbar'>
                                    {provGames.map(y => {
                                      if (y.gType !== 0 && y.gType !== 99) return false
                                      return (
                                        <li className='itemCarousel'>
                                          <img
                                            class='test'
                                            style={{ width: '100%', height: '100%' }}
                                            onClick={() => y.onClick(y, ctx.user, ctx)}
                                            src={`${imageUrl(y.image)}`}
                                            alt={y.name}
                                          />
                                          <span style={{ fontSize: api.isAppMobile ? '6pt' : '8pt' }}>{y.name}</span>
                                        </li>
                                      )
                                    })}
                                  </ul>
                                  {api.isAppMobile ? (
                                    ''
                                  ) : (
                                    <Button.Group
                                      className='slider-button'
                                      basic
                                      inverted
                                      style={{ border: '1px solid #616168' }}>
                                      <Button
                                        style={{ color: 'white' }}
                                        onClick={e => {
                                          e.preventDefault()
                                          scrollHighlight(sliderHLRefs.current[idx], -1)
                                        }}>
                                        <Icon name='arrow left' style={{ color: 'white' }} />
                                      </Button>
                                      <Button
                                        onClick={e => {
                                          e.preventDefault()
                                          scrollHighlight(sliderHLRefs.current[idx], 1)
                                        }}>
                                        <Icon name='arrow right' style={{ color: 'white' }} />
                                      </Button>
                                    </Button.Group>
                                  )}
                                </React.Fragment>
                              )
                            })}
                          </React.Fragment>
                        )
                      }
                      return (
                        <ul name='isRecommend' ref={xSliderisRecommend} className='hs hs2 full no-scrollbar'>
                          {shuffleGame.map(y => {
                            const poms = process.env.REACT_APP_HIGHLIGHT_PROVIDER.split(',')
                            for (var i = 0; i < poms.length; i++) {
                              if (y.provider === poms[i]) return false
                            }

                            return (
                              <li className='itemCarousel'>
                                <img
                                  class='test'
                                  style={{ width: '100%', height: '100%' }}
                                  onClick={() => y.onClick(y, ctx.user, ctx)}
                                  src={`${imageUrl(y.poster)}`}
                                  alt={y.name}
                                />
                              </li>
                            )
                          })}
                        </ul>
                      )
                    })()}
                  </div>
                </React.Fragment>
              )
            })}
          </React.Fragment>
        )
      })()}
    </div>
  )
}

export default GamesList
