import React, { useState, useContext, useEffect } from 'react'
import { Grid, Card, Image, Container, Label } from 'semantic-ui-react'
import api from '../../api'
import gamesTransformer from '../../transformers/main'
import { UserContext } from '../../contexts/User'
import LazyLoad from 'react-lazyload'

const HomeFC = () => {
  const [allGames, setAllGames] = useState([])
  const ctx = useContext(UserContext)
  const user = useContext(UserContext).user
  const preferredLocale = useContext(UserContext).preferredLocale

  useEffect(() => {
    const getGames = async () => {
      const GamesResponse = await api.getGamesList()
      const GamesJson = await GamesResponse.json()
      let Games = []
      GamesJson.games.forEach(g => {
        g.list.forEach(l => {
          if (l.isFC && user.isFC) {
            const gameItem = gamesTransformer(l, g.gType, user)
            Games.push(gameItem)
          }
          if (l.isPromo && user.isPromo) {
            const gameItem = gamesTransformer(l, g.gType, user)
            Games.push(gameItem)
          }
        })
      })
      setAllGames(Games)
    }
    getGames()
  }, [])

  return (
    <Container className='containerExpand' style={{ marginTop: 5 }}>
      <div style={{ textAlign: 'center' }}>
        <h2>{user.isFC ? 'PROMO #2' : 'PROMO #1'}</h2>
      </div>
      <Grid doubling columns={window.innerWidth < 1024 ? '10' : '5'}>
        {allGames.map(y => {
          return (
            <Grid.Column className='gridGames'>
              <Card className='imgList'>
                <LazyLoad>
                  <Image onClick={() => y.onClick(y, user, ctx)} src={y.image} />
                </LazyLoad>

                <Label className='gameLabel'>
                  {preferredLocale === 'th' ? y.nameth : y.name} {'  '}
                </Label>
              </Card>
            </Grid.Column>
          )
        })}
      </Grid>
    </Container>
  )
}

export default HomeFC
