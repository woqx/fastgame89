import React, { useState, useEffect, useContext } from 'react'
import {
  Card,
  Container,
  Grid,
  Image,
  Label,
  Loader,
  Icon,
  Dimmer,
  Input,
  Button,
  Dropdown,
  Divider,
} from 'semantic-ui-react'
import api from '../../api'
import GameCategory from '../../components/GameCategory'
import { UserContext } from '../../contexts/User'
import gamesTransformer from '../../transformers/main'
import { categoryCodes } from '../../config/games/index'
import Translate, { translate } from '../../locales/Translate'
import { Helmet } from 'react-helmet'
import jwt from 'jwt-decode'
import SportScreen from '../SportScreen'
import FavGame from '../../components/FavGame'
import LazyLoad from 'react-lazyload'
import axios from 'axios'
import GamesList from './GamesList'
import SlidersCustom from './Sliders'
import _ from 'lodash'
import '../../assets/Providers-dropdown.css'
import { pName } from '../../utils'

const HomeScreen = () => {
  // const [sites, setSites] = useState(null)
  const [homeState, setHomeState] = useState({
    showSport: false,
    activeGameCategory: Number(localStorage.getItem('activeCategory')) || 3,
    isLoading: false,
    allGames: null,
    searchTerm: '',
    filterProvider: 'All',
    favLoader: false,
  })
  const ctx = useContext(UserContext)

  // const getSitesInfo = async () => {
  //   const sites = await api.getBannersList()
  //   setSites(sites)
  // }

  const getGames = async first => {
    const GameResponse = await api.getGamesList()
    if (GameResponse.status === '500') {
      return false
    }
    const GamesJson = await GameResponse.json()
    let Games = []
    GamesJson.games.forEach(x => {
      x.list.forEach(y => {
        const gameItem = gamesTransformer(y, x.gType, ctx)
        Games.push(gameItem)
      })
    })

    const uniq_providers = _.uniqBy(Games, function(o) {
      return o.provider
    })
    uniq_providers.sort((a, b) => (a.provider > b.provider ? 1 : -1))
    const providers = uniq_providers
      .filter(up => {
        return up.provider !== undefined
      })
      .map(up => {
        const p = {
          key: up.provider,
          value: up.provider,
          text: window.pName(up.provider),
          image: { avatar: true, src: `https://ik.imagekit.io/xr5d8qn155ac5/Provider-logo/v2/${up.provider}.png` },
        }
        return p
      })

    providers.unshift({ key: 'All', value: 'All', text: 'All' })

    if (ctx.user && first) {
      await axios
        .post(`${process.env.REACT_APP_FAVORITE_LIST}/getFavList`, null, {
          params: {
            user: ctx.user.userName,
          },
        })
        .then(res => {
          res.data.list.map(x => {
            const favGames = JSON.parse(x)
            favGames.isFav = true
            const gameItem = gamesTransformer(favGames, 98, ctx)
            Games.push(gameItem)
          })
        })
        .catch(e => console.error(e))
    }

    setHomeState(prevState => ({
      ...homeState,
      Games,
      allGames: first ? [...Games] : prevState.allGames,
      isLoading: false,
      activeGameCategory: Number(localStorage.getItem('activeCategory')) || 4,
      showSport: false,
      providers,
      filterProvider: first ? 'All' : prevState.filterProvider,
    }))
  }

  const handleGameCategorySelection = async id => {
    localStorage.setItem('activeCategory', id)
    if (id === -2) return setHomeState({ ...homeState, showSport: true, activeGameCategory: id, provider: 'MJS' })
    setHomeState({ ...homeState, isLoading: true, activeGameCategory: id, showSport: false })
    getGames(false)
  }

  const setFavLoader = mode => {
    setHomeState(prevState => ({
      ...homeState,
      favLoader: !prevState.favLoader,
      favMode: mode,
    }))
  }

  const changeProvider = async (e, data) => {
    if (data.value === 'All') {
      getGames(true)
    }
    const fp = await homeState.Games.filter(x => {
      return x.provider === `${data.value}`
    })
    setHomeState({ ...homeState, allGames: fp, filterProvider: data.value })
  }

  useEffect(() => {
    // getSitesInfo()
    getGames(true)
  }, [])

  const selectedGames = homeState.searchTerm
    ? _.filter(homeState.allGames, x => {
        if (!x) return false

        return x.name.toLowerCase().search(homeState.searchTerm) !== -1
      })
    : _.filter(homeState.allGames, x => _.includes(x.categoryIds, homeState.activeGameCategory))

  return (
    <div>
      <UserContext.Consumer>
        {({ user, sitesInfo }) => {
          return (
            <React.Fragment>
              <Helmet>
                <title>{sitesInfo && sitesInfo.landingpage && sitesInfo.landingpage.title}</title>
                <meta
                  name='description'
                  content={sitesInfo && sitesInfo.landingpage && sitesInfo.landingpage.description}
                />
              </Helmet>
              {/* BANNER AREA */}
              {!user && <SlidersCustom banners={sitesInfo && sitesInfo.bannerArray} bannerSlide={true} />}
            </React.Fragment>
          )
        }}
      </UserContext.Consumer>

      {/*END BANNER AREA */}
      {/* CATEGORY AREA */}
      <div
        style={{
          backgroundImage:
            'linear-gradient(to top, rgb(59, 7, 71), rgb(67, 10, 80), rgb(75, 13, 90), rgb(83, 16, 99), rgb(91, 19, 109))',
        }}>
        <Container style={{ position: 'relative', top: 10 }}>
          <Grid>
            <Grid.Row style={{ padding: '0' }}>
              <Grid.Column style={{ padding: '0', paddingTop: '0px' }}>
                <div className='catCSS'>
                  <SlidersCustom
                    categorySlide={true}
                    activeGameCategory={homeState.activeGameCategory}
                    categoryCodes={categoryCodes}
                    changeCategory={handleGameCategorySelection}
                    ctx={ctx}
                  />
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </div>
      {/*END  CATEGORY AREA */}

      {/* GamesList Area */}
      {(() => {
        if (homeState.isLoading) {
          return (
            <Container className='containerExpand'>
              <Loader active inline='centered' size='big'>
                Loading
              </Loader>
            </Container>
          )
        }
        if (homeState.showSport) {
          return <SportScreen provider={homeState.provider} />
        }
        return (
          <React.Fragment>
            <Container className='posterGames' style={{ marginTop: 10, marginBottom: 10 }}>
              <div>
                <GamesList
                  allGames={homeState.allGames}
                  G={homeState.Games}
                  activeGameCategory={homeState.activeGameCategory}
                  ctx={ctx}
                />
              </div>
            </Container>
            <Container className='containerExpand' style={{ marginTop: 5, marginBottom: 20 }}>
              {/* New games list */}
              <Grid stackable columns={2}>
                <Grid.Column>
                  <Input
                    icon
                    placeholder={translate(ctx.preferredLocale, 'home.search')}
                    fluid
                    onChange={e => setHomeState({ ...homeState, searchTerm: e.target.value.toLocaleLowerCase() })}>
                    <input value={homeState.searchTerm} />
                    <Icon name='search' />
                  </Input>
                </Grid.Column>
                <Grid.Column>
                  <Dropdown
                    inverted
                    className='pvd-dropdown'
                    placeholder='Filter by providers'
                    fluid
                    selection
                    options={homeState.providers}
                    value={homeState.filterProvider}
                    onChange={changeProvider}
                  />
                </Grid.Column>
              </Grid>
              <p></p>
              <Divider />
              <div>
                {selectedGames && (
                  <Grid doubling columns={window.innerWidth < 1024 ? '10' : '5'}>
                    {selectedGames.map(y => {
                      if (y.isRecommend) {
                        return false
                      }
                      if (y.isNew) {
                        return false
                      }

                      const poms = process.env.REACT_APP_HIGHLIGHT_PROVIDER.split(',')
                      for (var i = 0; i < poms.length; i++) {
                        if (y.provider === poms[i] && (y.gType === 0 || y.gType === 99)) return false
                      }

                      return (
                        <Grid.Column className={'gridGames'}>
                          <Card className={'imgList'}>
                            <div
                              className='logo-providers-sm'
                              style={{
                                zIndex: '1',
                                position: 'absolute',
                                height: 'fit-content',
                                backgroundColor: 'rgba(0, 0, 0, 0.65)',
                                padding: '0px 10px 0px 10px',
                                top: '4px',
                                left: '5px',
                              }}>
                              <span style={{ fontSize: api.isAppMobile ? '0.6rem' : '1rem' }}>
                                {window.pName(y.provider)}
                              </span>
                            </div>
                            <LazyLoad>
                              <Image onClick={() => y.onClick(y, ctx.user, ctx)} src={y.image} />
                            </LazyLoad>

                            <Label className={'gameLabel'}>
                              {(() => {
                                if (ctx.user) {
                                  if (homeState.activeGameCategory === 2) {
                                    if (y.isFav) {
                                      return (
                                        <FavGame
                                          game={y}
                                          reload={() => {
                                            getGames(true)
                                          }}
                                          favLoader={setFavLoader}
                                          user={ctx.user ? jwt(api.getCurrentUser().token).username : ''}
                                        />
                                      )
                                    }
                                  } else {
                                    return (
                                      <FavGame
                                        game={y}
                                        reload={() => {
                                          getGames(true)
                                        }}
                                        favLoader={setFavLoader}
                                        user={ctx.user ? jwt(api.getCurrentUser().token).username : ''}
                                      />
                                    )
                                  }
                                }
                              })()}
                              {ctx.preferredLocale === 'th' ? y.nameth : y.name} {'  '}
                            </Label>
                          </Card>
                        </Grid.Column>
                      )
                    })}
                  </Grid>
                )}
              </div>
            </Container>
          </React.Fragment>
        )
      })()}
      {/*END GamesList Area */}
      {/* favLoader Dimmer */}
      <Dimmer.Dimmable blurring dimmed={homeState.favLoader}>
        <Dimmer active={homeState.favLoader} inverted page>
          <Loader active={homeState.favLoader} inline='centered' size='massive'>
            <Translate string={`favLoading${homeState.favMode}`} />
            <br />
            <br />

            <Button
              icon='refresh'
              size='big'
              color='red'
              content='Refresh'
              labelPosition='left'
              onClick={e => {
                e.preventDefault()
                window.location.reload()
              }}
            />
          </Loader>
        </Dimmer>
      </Dimmer.Dimmable>
      {/* End favLoader Dimmer */}
    </div>
  )
}

export default HomeScreen
