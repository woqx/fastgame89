import React, { Component, useRef, useEffect } from 'react'
import Slider from 'react-slick'
import { Image } from 'semantic-ui-react'
import GameCategory from '../../components/GameCategory'

export default ({ banners, bannerSlide, categorySlide, categoryCodes, activeGameCategory, changeCategory, ctx }) => {
  const isAppMobile = /Mobi/.test(navigator.userAgent)
  let { slider, catSlider } = useRef(null)

  // useEffect(() => {
  //   return () => {}
  // }, [])
  const bannerSettings = {
    infinite: true,
    speed: 1000,
    slidesToShow: 2,
    initialSlide: 0,
    // centerMode: true,
    // centerPadding: '10px',
    slidesToScroll: 1,
    // lazyLoad: true,
    swipeToSlide: true,
    autoplaySpeed: 3000,
    dots: true,
    arrows: false,
    adaptiveHeight: false,
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  }

  const categorySettings = {
    lazyLoad: false,
    slidesToShow: 7,
    infinite: false,
    swipeToSlide: true,
    dots: false,
    arrows: true,
    // beforeChange: (current, next) => this.setState({ slideIndex: next }),
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: false,
        },
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: false,
        },
      },
    ],
  }
  const _onVideoPlay = e => {
    slider.slickPause()
  }

  const _onVideoEnd = e => {
    slider.slickPlay()
  }

  if (bannerSlide) {
    return (
      <Slider ref={c => (slider = c)} {...bannerSettings} autoplay={true} className='bannerStyle'>
        {banners &&
          banners.map((y, i) => {
            return (
              <div key={i}>
                {y.video ? (
                  <video
                    id='SlideDiv'
                    playsInline
                    muted
                    onPlay={_onVideoPlay}
                    autoPlay
                    onEnded={_onVideoEnd}
                    src={y.video}
                    className='bannerVideo'
                  />
                ) : (
                  <Image id='SlideDiv' src={y.image} key={i} alt={y.altIMG} className='bannerStyle' />
                )}
              </div>
            )
          })}
      </Slider>
    )
  }

  if (categorySlide) {
    return (
      <Slider ref={slider => (catSlider = slider)} {...categorySettings} autoplay={false}>
        {categoryCodes.map(x => (
          <GameCategory
            name={x.name}
            isActive={Number(activeGameCategory) === x.id}
            onClick={() => changeCategory(x.id)}
            activeImage={x.activeIcon}
            image={x.icon}
            ctx={ctx}
          />
        ))}
      </Slider>
    )
  }
}
