import React from 'react'
import api from '../api'
import { Button, Container, Form, Message, Grid, Header, Card } from 'semantic-ui-react'
import { Formik } from 'formik'
import Translate from '../locales/Translate'
import * as Yup from 'yup'
import * as _ from 'lodash'
import { UserContext } from '../contexts/User'
import { Helmet } from 'react-helmet'

class ChangePasswordScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      notSame: false,
    }
  }

  ChangePasswordSchema = Yup.object().shape({
    password: Yup.string()
      .required('Password is required')
      .matches(/^[0-9]{6,6}$/, {
        excludeEmptyString: true,
        message: 'Password 6 angka',
      }),
    confirmPassword: Yup.string()
      .required('Confirm Password is required')
      .matches(/^[0-9]{6,6}$/, {
        excludeEmptyString: true,
        message: 'Password 6 angka',
      }),
  })

  render() {
    return (
      <Container className='containerExpand'>
        <Helmet>
          <title>Ganti Password</title>
          <meta name='description' content='Change your password frequently, to secure your account at QQDEWA' />
        </Helmet>
        <Card className='custom-form'>
          <Grid style={{ height: '100%' }} verticalAlign='middle'>
            <Grid.Column>
              <Header as='h2' style={{ color: '#0b8040' }} className='form-header' textAlign='center'>
                <Translate string={'changePassword.heading'} />
              </Header>
              <Formik
                validationSchema={this.ChangePasswordSchema}
                onSubmit={(values, { setSubmitting }) => {
                  setSubmitting(true)
                  if (values.password !== values.confirmPassword) {
                    this.setState({ notSame: true })
                    setSubmitting(false)
                  } else {
                    this.setState({ notSame: false })
                    api
                      .changePassword(values.password)
                      .then(res => {
                        window.location.href = '/'
                        setSubmitting(false)
                      })
                      .catch(e => {
                        setSubmitting(false)
                      })
                  }
                }}>
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  /* and other goodies */
                }) => (
                  <Form loading={isSubmitting} onSubmit={handleSubmit} error={errors}>
                    <Form.Group widths='equal'>
                      <Form.Field
                        value={values.password}
                        name='password'
                        type='password'
                        onChange={handleChange}
                        onBlur={handleBlur}
                        pattern='[0-9]*'
                        inputmode='numeric'
                        required
                        label={
                          <label>
                            <Translate string={'changePassword.password'} />
                          </label>
                        }
                        control='input'
                      />
                    </Form.Group>
                    <Form.Group widths='equal'>
                      <Form.Field
                        value={values.confirmPassword}
                        name='confirmPassword'
                        type='password'
                        onChange={handleChange}
                        onBlur={handleBlur}
                        pattern='[0-9]*'
                        inputmode='numeric'
                        required
                        label={
                          <label>
                            <Translate string={'changePassword.confirmPassword'} />
                          </label>
                        }
                        control='input'
                      />
                    </Form.Group>
                    {!this.state.notSame ? null : <Message error header='Please Try Again' content={'Not Matching'} />}
                    {!this.state.notWorking ? null : <Message error header='Failed' content={'Please contact us'} />}
                    <Button
                      type='submit'
                      onClick={handleSubmit}
                      style={{ backgroundColor: '#0b8040', color: 'white' }}
                      fluid
                      size='large'>
                      <Translate string={'submit'} />
                    </Button>
                    <Message error hidden={_.isEmpty(errors)} header='Errors' content={JSON.stringify(errors)} />
                  </Form>
                )}
              </Formik>
            </Grid.Column>
          </Grid>
        </Card>
      </Container>
    )
  }
}

ChangePasswordScreen.contextType = UserContext

export default ChangePasswordScreen
