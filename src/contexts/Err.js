import React from 'react'

export const ErrContext = React.createContext({
  showErr: false,
  ErrMessage: 'test',
  setErr: () => {},
})
