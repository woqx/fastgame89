import React, { Component } from 'react'
import { LocaleContext } from '../contexts/Locale'

import en from './en.json'
import th from './th.json'
import id from './id.json'

export default class Translate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      langs: {
        id,
        en,
        th,
      },
    }
  }
  render() {
    const { langs } = this.state
    const { string } = this.props
    return (
      <LocaleContext.Consumer>
        {({ preferredLocale, changeLanguage }) => {
          // console.log(value)
          // console.log(langs[value][string])
          return langs[preferredLocale][string]
        }}
      </LocaleContext.Consumer>
    )
  }
}

export const translate = (locale, key) => {
  const langs = {
    id,
    en,
    th,
  }
  return langs[locale][key]
}
