import { mapper } from '../config/games/games'
import App from '../App'

const { REACT_APP_IMAGE_URL, REACT_APP_HABANERO_IMAGE_URL } = process.env
const onClick = (game, user, gType, ctx) => {
  if (user) {
    App.gameLoader(ctx)
    window.LaunchGames(
      App,
      ctx,
      game,
      false,
      process.env.REACT_APP_BACKEND_URL,
      process.env.REACT_APP_JDB_PARENT,
      process.env.REACT_APP_HABANERO_BRANDID,
      process.env.REACT_APP_BSG_BANKID,
    )
  } else {
    App.gameLoader(ctx)

    if (game.provider === 'PRAG') {
      const lang = ctx.preferredLocale
      App.gameLoader(ctx)

      window.location.href = `https://demogamesfree-asia.pragmaticplay.net/gs2c/openGame.do?lang=${lang}&cur=THB&gameSymbol=${game.id}&jurisdiction=UK&lobbyURL=${window.location.href}&stylename=${process.env.REACT_APP_PRAG_SECURELOGIN}`
      return
    }

    if (
      game.provider === 'RED' ||
      game.provider === 'QTG' ||
      game.provider === 'PGS' ||
      game.provider === 'YGG' ||
      game.provider === 'ISB' ||
      game.provider === 'GNS' ||
      game.provider === 'R8' ||
      game.provider === 'BFX' ||
      game.provider === 'haba' ||
      game.provider === 'MJG' ||
      game.provider === 'BSG' ||
      game.provider === 'GMT' ||
      game.provider === 'PNG' ||
      game.provider === 'BBT' ||
      game.provider === 'APG'
    ) {
      window.LaunchGames(
        App,
        ctx,
        game,
        true,
        process.env.REACT_APP_BACKEND_URL,
        process.env.REACT_APP_JDB_PARENT,
        process.env.REACT_APP_HABANERO_BRANDID,
      )
      return
    }
    App.gameLoader(ctx)
    App.showErr(ctx, 'err.PleaseLogin')
  }
}

const transform = (game, gType, ctx) => {
  var categoryIds = []
  // if (_.includes(blacklistedGames, game.mType.toString())) {
  // 	return false;
  // }

  categoryIds.push(mapper[gType])
  // if (!game.isShow || game.isShow === undefined) {
  //   return false
  // }

  if (process.env.REACT_APP_SHOW_HIDDEN === 'show') {
    if (game.isShow) {
      console.log('here')
      return false
    }
  } else {
    if (!game.isShow || game.isShow === undefined) {
      return false
    }
  }

  let imageUrl = `${REACT_APP_IMAGE_URL}${game.image}`
  if (game.provider === 'JDB') {
    if (!game.image.includes('/thumbnail/')) {
      imageUrl = game.image
    }
  }
  if (game.provider === 'PRAG') {
    imageUrl = game.image
  }
  if (game.provider === 'haba') {
    imageUrl = `${REACT_APP_HABANERO_IMAGE_URL}${game.image}.png`
  }

  return {
    provider: game.provider,
    id: game.mType,
    name: game.name,
    nameth: game.nameth,
    categoryIds,
    gType,
    image: imageUrl,
    isPromo: game.isPromo,
    isShowJP: game.isShowJP,
    isFav: game.isFav,
    isNew: game.isNew,
    isRecommend: game.isRecommend,
    poster: game.poster,
    onClick: (game, user, ctx) => onClick(game, user, gType, ctx),
  }
}

export default transform
