import React, { Component } from 'react'
import { Menu, Icon, Responsive, List } from 'semantic-ui-react'
import { UserContext } from '../contexts/User'
import CurrencyFormat from 'react-currency-format'
import Translate from '../locales/Translate'
import { NavLink } from 'react-router-dom'

const BottomNavigation = props => (
  <div className={'bottomNavigation'}>
    <Responsive maxWidth={Responsive.onlyMobile.maxWidth}>
      <UserContext.Consumer>
        {({ user }) =>
          user ? (
            <Menu
              icon='labeled'
              borderless
              widths={4}
              style={{
                flexShrink: 0, //don't allow flexbox to shrink it
                borderRadius: 0, //clear semantic-ui style
                margin: 0, //clear semantic-ui style
              }}>
              <Menu.Item
                style={{ borderRight: '1px solid #2b2b2b' }}
                as='a'
                className='textlight-white-mobile'
                href='/profile'>
                <Icon name='exchange' />
                <CurrencyFormat
                  value={Number(Math.floor(100 * user.AvailableCredit) / 100).toFixed(2)}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp'}
                />
                <small>{user.isPromo ? 'Pro #1' : user.isFC ? 'Pro #2' : ''}</small>
              </Menu.Item>
              <Menu.Item
                style={{ borderRight: '1px solid #2b2b2b' }}
                as='a'
                className='textlight-white-mobile'
                href='/statement'>
                <Icon name='database' />
                <Translate string={'navbar.statement'} />
              </Menu.Item>
              <Menu.Item
                style={{ borderRight: '1px solid #2b2b2b' }}
                as='a'
                className='textlight-white-mobile'
                href='/changePassword'>
                <Icon name='key' />
                <Translate string={'navbar.changePassword'} />
              </Menu.Item>
              <Menu.Item
                as='a'
                className='textlight-white-mobile'
                href='https://secure.livechatinc.com/licence/11793483/v2/open_chat.cgi?license=11793483&group=0&embedded=1&widget_version=3&unique_groups=0'>
                <Icon name='comments outline' />
                liveChat
              </Menu.Item>
            </Menu>
          ) : (
            <Menu
              icon='labeled'
              borderless
              widths={3}
              style={{
                flexShrink: 0, //don't allow flexbox to shrink it
                borderRadius: 0, //clear semantic-ui style
                margin: 0, //clear semantic-ui style
              }}>
              <Menu.Item as='a' className='textlight-white-mobile' href='/'>
                <Icon name='home' />
              </Menu.Item>

              <Menu.Item as='a' className='textlight-white-mobile' href='/promo'>
                <Icon name='gift' />
                <Translate string={'navbar.promo'} />
              </Menu.Item>
              <Menu.Item
                as='a'
                className='textlight-white-mobile'
                href='https://secure.livechatinc.com/licence/11793483/v2/open_chat.cgi?license=11793483&group=0&embedded=1&widget_version=3&unique_groups=0'>
                <Icon name='comments outline' />
                LiveChat
              </Menu.Item>
            </Menu>
          )
        }
      </UserContext.Consumer>
    </Responsive>
  </div>
)

export default BottomNavigation
