import React, { Component } from 'react'
import { Icon, Container, Segment, Grid, GridColumn, List, Image } from 'semantic-ui-react'
import Translate from '../locales/Translate'
import { LocaleContext } from '../contexts/Locale'
import thFlag from '../assets/images/thailand.svg'
import enFlag from '../assets/images/united-kingdom.svg'
import idFlag from '../assets/images/indonesia.svg'
import { faWhatsapp, faLine, faWeixin, faSkype, faTelegram } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import api from '../api'

const providers = [
  '1X2',
  'AGG',
  'BB',
  'BF',
  'BG',
  'BMS',
  'BP',
  'BTD',
  'CI',
  'DGS',
  'ELK',
  'ESC',
  'EVO',
  'EZG',
  'FGS',
  'GFG',
  'GL',
  'GT',
  'HBN',
  'HSG',
  'ID',
  'ION',
  'ISB',
  'KLB',
  'KR',
  'LBG',
  'M',
  'MBL',
  'MJ',
  'NG',
  'NLM',
  'OMI',
  'OT',
  'PG',
  'PMT',
  'PTGN',
  'QS',
  'RD',
  'RT',
  'RTG',
  'RVG',
  'SA',
  'SBCR',
  'SG',
  'SM',
  'SR',
  'TDK',
  'WH',
  'YGG',
]
export default class Footer extends Component {
  render() {
    return (
      <div class='ui inverted vertical footer segment' className='customFooter' style={{ height: '100%' }}>
        <div class='link-center'></div>

        <Container>
          <Grid stackable>
            <Grid.Column width={12} textAlign='left'>
              <Segment inverted compact>
                <List horizontal>
                  {['id', 'en', 'th'].map(lang => {
                    return (
                      <List.Item>
                        <LocaleContext.Consumer>
                          {({ preferredLanguage, changeLanguage }) => (
                            <Image
                              size='mini'
                              src={lang === 'en' ? enFlag : lang === 'th' ? thFlag : idFlag}
                              style={{ cursor: 'pointer' }}
                              onClick={() => changeLanguage(lang)}
                              positive={preferredLanguage === lang}
                            />
                          )}
                        </LocaleContext.Consumer>
                      </List.Item>
                    )
                  })}
                </List>
              </Segment>
              <Segment inverted>
                                  <p>
                  <h1>Situs Game Slots Online Terbaru</h1>
                  Fastgames89 adalah situs permainan online pertama di Indonesia yang menyediakan aneka permainan
                  bervariasi seperti Puzzle Bubble Online / Tembak Bola Online, Ular Tangga, Game Slots, Tembak Ikan,
                  Racing Game, Bingo, Keno, dan masih banyak lagi.
                </p>

                <p>
                  <h2>Daftar Slot Online Puzzle Bubble, Tembak Bola Terbaik FastGames89</h2>
                  Website kami menggunakan sistem seamless tercanggih, akses paling cepat, 100% aman dan privasi
                  terjamin. Proses pendaftaran yang simple dan sistem transaksi yang praktis dan otomatis.
                </p>
                <p>
                  Segera daftar sekarang dan menangkan hadiah jackpot, hadiah yang anda menangkan dapat langsung di
                  withdraw. Website kami selalu aktif 24 /7 tanpa libur.
                </p>
                <p>
                  <h3>Nikmati Permainan Slot Online Mobile Dan Computer Dengan Mudah</h3>
                  Teknologi yang kami gunakan dapat mempermudah anda untuk mengakses permainan kapanpun dan dimanapun.
                  Anda tidak perlu mendownload, semua game dapat langsung di klik dan dimainkan. Anda bebas mengakses
                  permainan di laptop, computer, tablet, android dan iPhone.
                </p>
                <p>Ayo bergabung sekarang juga, silahkan hubungi kami apabila ada pertanyaan.</p>
                <h2>ENJOY THE GAME AND WIN THE MONEY</h2>

                <Grid columns='5' style={{ marginTop: '30px' }}>
                  {providers.map(p => (
                    <Grid.Column className='bg-provider-footer'>
                      <Image
                        size={api.isAppMobile ? 'tiny' : ''}
                        src={`https://ik.imagekit.io/xr5d8qn155ac5/Provider-logo/${p}.png`}
                      />
                    </Grid.Column>
                  ))}
                </Grid>
              </Segment>
            </Grid.Column>
            <Grid.Column width={4}>
                                <Segment inverted>
                <h2>Hubungi kami</h2>
                <List inverted verticalAlign='middle'>
                  <List.Item>
                    <FontAwesomeIcon icon={faWhatsapp} size='2x' style={{ paddingRight: '5px' }} />{' '}
                    <a href='https://wa.me/6281779536396'>+6281779536396</a>
                  </List.Item>
                  <List.Item>
                    <FontAwesomeIcon icon={faWeixin} size='2x' style={{ paddingRight: '5px' }} /> fastgames89
                  </List.Item>
                  <List.Item>
                    <FontAwesomeIcon icon={faLine} size='2x' style={{ paddingRight: '5px' }} />{' '}
                    <a href='http://line.me/ti/p/~fastgames89'>fastgames89</a>
                  </List.Item>
                  <List.Item>
                    <FontAwesomeIcon icon={faTelegram} size='2x' style={{ paddingRight: '5px' }} />
                    <a href='https://t.me/fastgames89'>fastgames89</a>
                  </List.Item>
                </List>
              </Segment>
              <Segment inverted>
                <List>
                  <List.Item as='a' href='/promo'>
                    <Icon name='gift' color='purple' />
                    Promo
                  </List.Item>
                  <List.Item>Game Slots</List.Item>
                  <List.Item>Slot Online</List.Item>
                  <List.Item>Tembak Bola</List.Item>
                  <List.Item>Puzzle Bubble</List.Item>
                  <List.Item>Ular Tangga</List.Item>
                  <List.Item>Tembak Ikan</List.Item>
                  <List.Item>Keno</List.Item>
                  <List.Item>Bingo</List.Item>
                  <List.Item>Livecasino</List.Item>
                </List>
              </Segment>
            </Grid.Column>
          </Grid>
        </Container>

        <div class='ui center aligned container'>
          <div class='ui inverted section divider'></div>
          <a href='/'>{process.env.REACT_APP_MAIN_PARENT.toUpperCase()}</a>
          <small>ⓒ 2019. All Right Reserved | 18+ (Powered by daVinci Gaming)</small>
          <div class='ui horizontal inverted small divided link list'></div>
        </div>
      </div>
    )
  }
}
