import React, { Component } from 'react'
import CurrencyFormat from 'react-currency-format'
import { Dropdown, Icon, Image, Menu, Responsive } from 'semantic-ui-react'
import { UserContext } from '../contexts/User'
import { LocaleContext } from '../contexts/Locale'
import Translate from '../locales/Translate'
import thFlag from '../assets/images/thailand.svg'
import enFlag from '../assets/images/united-kingdom.svg'
import idFlag from '../assets/images/indonesia.svg'
import { HomeContext } from '../contexts/home-context'

const langs = [
  {
    key: 'id',
    text: 'Indonesia',
    value: 'id',
    image: { avatar: true, src: idFlag },
  },
  {
    key: 'en',
    text: 'English',
    value: 'en',
    image: { avatar: true, src: enFlag },
  },
  {
    key: 'th',
    text: 'ไทย',
    value: 'th',
    image: { avatar: true, src: thFlag },
  },
]

const MobileNavbar = ({ user, logout }) => (
  <Menu fixed>
    <Menu.Item as='a' header as='a' href='/'>
      <Image size='small' src='https://ik.imagekit.io/xr5d8qn155ac5/fastgames89/logo.png' />
    </Menu.Item>
    {!user ? (
      <Menu.Menu position='right'>
        <Menu.Item as='a' className='textlight-white' href='/register'>
          <Icon name='signup' />
          <Translate string={'navbar.register'} />
        </Menu.Item>
        <Menu.Item as='a' className='textlight-white' href='/login'>
          <Icon name='sign-in' />
          <Translate string={'navbar.login'} />
        </Menu.Item>
      </Menu.Menu>
    ) : (
      <Menu.Menu position='right'>
        <Menu.Item as='a' className='textlight-white' href='/promo'>
          <Icon name='gift' />
          <Translate string={'navbar.promo'} />
        </Menu.Item>
        <Menu.Item as='a' className='textlight-white' onClick={logout} href='/'>
          <Icon name='sign-out' />
          <Translate string={'navbar.logout'} />
        </Menu.Item>
      </Menu.Menu>
    )}
  </Menu>
)

const TabletNavbar = ({ user, logout }) => (
  <Menu>
    <Menu.Item as='a' header href='/'>
      <Image size='small' src='https://ik.imagekit.io/xr5d8qn155ac5/fastgames89/logo.png' />
    </Menu.Item>
    {!user ? (
      <Menu.Menu position='right'>
        <Dropdown item icon={'align justify'} text={'Menu  '} className='textlight-white'>
          <Dropdown.Menu>
            <Dropdown.Item as='a' className='textlight-white' href='/login'>
              <Icon name='sign-in' />
              <Translate string={'navbar.login'} />
            </Dropdown.Item>
            <Dropdown.Item as='a' className='textlight-white' href='/register'>
              <Icon name='signup' />
              <Translate string={'navbar.register'} />
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Menu.Menu>
    ) : (
      <Menu.Menu position='right'>
        <Dropdown fluid item icon={'align justify'} text={'Menu  '} className='textlight-white'>
          <Dropdown.Menu>
            <Dropdown.Item as='a' href='/changePassword'>
              <Icon name='user' /> {user.userName}
            </Dropdown.Item>
            <Dropdown.Item as='a' href='/profile'>
              <Icon name='money' />
              <CurrencyFormat
                value={Number(Math.floor(100 * user.AvailableCredit) / 100).toFixed(2)}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp'}
              />
            </Dropdown.Item>

            <Dropdown.Item as='a' href='/statement'>
              <Icon name='database' />
              <Translate string={'navbar.statement'} />
            </Dropdown.Item>
            {/* <Dropdown.Item as='a' className='textlight-white' href=''>
              <Icon name='comments outline' size='large' />
              <Translate string={'navbar.line@'} />
            </Dropdown.Item> */}
            <Dropdown.Item as='a' onClick={logout} href='/'>
              <Icon name='sign-out' />
              <Translate string={'navbar.logout'} />
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Menu.Menu>
    )}
  </Menu>
)

const DesktopNavbar = ({ user, logout, changeLanguage }) => (
  <Menu>
    {!user ? (
      <div class='table'>
        <div class='tr'>
          <div class='th' style={{ display: '-webkit-box' }}>
            <Menu.Menu position='right'>
              <Menu.Item as='a' className='textlight-white' href='/login'>
                <Icon name='sign-in' size='large' />
                <Translate string={'navbar.login'} />
              </Menu.Item>
              <Menu.Item as='a' className='textlight-white' href='/register'>
                <Icon name='signup' size='large' />
                <Translate string={'navbar.register'} />
              </Menu.Item>
            </Menu.Menu>
          </div>
        </div>
        <div class='tr'>
          <div class='td' style={{ display: '-webkit-box' }}>
            <Menu.Menu position='right'>
              <Menu.Item className='textlight-white' as='a' href='/promo'>
                <Translate string={'navbar.promo'} />
              </Menu.Item>
            </Menu.Menu>
          </div>
        </div>
      </div>
    ) : (
      <React.Fragment>
        <div class='table'>
          <div class='tr'>
            <div class='th' style={{ display: '-webkit-box' }}>
              <Menu.Menu position='right'>
                <Menu.Item className='textlight-white' as='a' href='/changePassword'>
                  <Icon name='user' size='large' /> {user.userName}
                </Menu.Item>

                <Menu.Item as='a' className='textlight-white' href='/profile'>
                  <Icon name='money' size='large' />
                  <CurrencyFormat
                    value={Number(Math.floor(100 * user.AvailableCredit) / 100).toFixed(2)}
                    displayType={'text'}
                    thousandSeparator={true}
                  />
                  <small>&nbsp;{user.isPromo ? ' Pro #1' : user.isFC ? ' Pro 2' : ''}</small>
                </Menu.Item>
                <Menu.Item className='textlight-white' as='a' href='/statement'>
                  <Icon name='database' size='large' />
                  <Translate string={'navbar.statement'} />
                </Menu.Item>

                <Menu.Item as='a' className='textlight-white' onClick={logout} href='/'>
                  <Icon name='sign-out' size='large' />
                  <Translate string={'navbar.logout'} />
                </Menu.Item>
              </Menu.Menu>
            </div>
          </div>

          <div class='tr'>
            <div class='td' style={{ display: '-webkit-box' }}>
              <Menu.Menu position='right'>
                <Menu.Item className='textlight-white' as='a' href='/promo'>
                  <Translate string={'navbar.promo'} />
                </Menu.Item>
              </Menu.Menu>
            </div>
          </div>
        </div>
      </React.Fragment>
    )}
  </Menu>
)

export default class NavBar extends Component {
  render() {
    return (
      <UserContext.Consumer>
        {({ user, logout }) => (
          <LocaleContext.Consumer>
            {({ changeLanguage }) => (
              <HomeContext.Consumer>
                {({ homeScreen, changeHomeScreen }) => (
                  <div>
                    <Responsive minWidth={Responsive.onlyComputer.minWidth}>
                      <section class='sec-test'>
                        <div class='left-test'>
                          <Menu.Item as='a' header href='/'>
                            <Image
                              size='small'
                              style={{ width: '200px', marginTop: '10px' }}
                              src='https://ik.imagekit.io/xr5d8qn155ac5/fastgames89/logo.png'
                            />
                          </Menu.Item>
                        </div>
                        <div class='right-test'>
                          <DesktopNavbar user={user} logout={logout} changeLanguage={changeLanguage} />
                        </div>
                      </section>
                    </Responsive>
                    <Responsive {...Responsive.onlyTablet}>
                      <TabletNavbar
                        user={user}
                        logout={logout}
                        homeScreen={homeScreen}
                        changeHomeScreen={changeHomeScreen}
                      />
                    </Responsive>
                    <Responsive {...Responsive.onlyMobile}>
                      <MobileNavbar user={user} changeLanguage={changeLanguage} logout={logout} />
                    </Responsive>
                  </div>
                )}
              </HomeContext.Consumer>
            )}
          </LocaleContext.Consumer>
        )}
      </UserContext.Consumer>
    )
  }
}
