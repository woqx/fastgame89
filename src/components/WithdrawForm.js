import React, { useContext } from 'react'
import api from '../api'
import { Button, Form, Message } from 'semantic-ui-react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import * as _ from 'lodash'
import Translate from '../locales/Translate'
import { UserContext } from '../contexts/User'

const WithdrawSchema = Yup.object().shape({
  amount: Yup.number()
    .positive('must positive values')
    .required()
    .min(process.env.REACT_APP_MIN_WD),
})

const WithdrawForm = ({ userBank }) => {
  const ctx = useContext(UserContext)

  return (
    <Formik
      validationSchema={WithdrawSchema}
      initialValues={{
        userName: ctx.user.userName,
        amount: null,
      }}
      onSubmit={(values, { setSubmitting }) => {
        if (values.amount <= ctx.user.AvailableCredit) {
          api
            .withdrawMoney(ctx.user.userName, values.amount)
            .then(x => x.json())
            .then(updatedInfo => {
              // console.log(updatedInfo)

              setSubmitting(false)
              ctx.setErr(true, `Success withdraw ${values.amount}`)
              values.amount = 0
            })
            .catch(e => console.error(e))
        } else {
          values.amount = 0
          setSubmitting(false)

          ctx.setErr(true, `you cannot withdraw more than ${ctx.user.AvailableCredit}`)
        }
      }}>
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        /* and other goodies */
      }) => (
        <Form loading={isSubmitting} onSubmit={handleSubmit} error={errors}>
          <Form.Group widths='equal'>
            <Form.Field
              value={Number(Math.floor(100 * ctx.user.AvailableCredit) / 100).toFixed(2)}
              name='credits'
              onChange={handleChange}
              onBlur={handleBlur}
              required
              label='Current Balance'
              disabled
              control='input'
            />
          </Form.Group>

          <Form.Group widths='equal'>
            <Form.Field
              value={userBank.bankAccount}
              name='bankAccount'
              onBlur={handleBlur}
              required
              label='Bank Account'
              disabled
              control='input'
            />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Field
              value={values.amount}
              name='amount'
              placeholder='Amount to withdraw?'
              onChange={handleChange}
              onBlur={handleBlur}
              required
              label={
                <label>
                  <Translate string='profile.amount' />
                </label>
              }
              control='input'
            />
          </Form.Group>
          {touched.amount && errors.amount && <span className={'formError'}> {errors.amount} </span>}
          <Button
            type='submit'
            onClick={handleSubmit}
            style={{ backgroundColor: '#0b8040', color: 'white' }}
            fluid
            size='large'>
            <Translate string={'profile.withdraw'} />
          </Button>

          <Message
            info
            hidden={!ctx.user.isPromo}
            header='Promo'
            content={
              <label>
                <Translate string='profile.promoWithdraw' />
              </label>
            }
          />
        </Form>
      )}
    </Formik>
  )
}

export default WithdrawForm
