import React, { useState, useRef, useContext } from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import * as _ from 'lodash'
import Translate, { translate } from '../locales/Translate'
import api from '../api'
import { Button, Form, Message, Grid, Image, List, Segment, Popup, Divider } from 'semantic-ui-react'
import { UserContext } from '../contexts/User'
import { update } from 'lodash'

const ProfileSchema = Yup.object().shape({
  amount: Yup.number()
    .required()
    .min(process.env.REACT_APP_MIN_DEPO),
  transferBy: Yup.string().required('Harap pilih jenis pembayaran'),
})

const DepositForm = ({ banks, userName, userBank }) => {
  const [imageFile, setImageFile] = useState()
  const [imagePreviewUrl, setImagePreview] = useState()
  const [uploading, setUpload] = useState(false)
  const [isOpen, setIsOpen] = useState(false)
  const fileInputRef = useRef(null)
  let intervalRef = useRef(null)
  const ctx = useContext(UserContext)

  const onFileUpload = e => {
    try {
      e.preventDefault()
      let reader = new FileReader()
      let file = e.target.files[0]
      reader.onloadend = () => {
        setImageFile(file)
        setImagePreview(reader.result)
      }
      reader.readAsDataURL(file)
    } catch (e) {
      console.warn(e)
    }
  }

  const handleOpen = bankAccount => {
    setIsOpen(true)
    const textField = document.createElement('textarea')
    textField.value = bankAccount
    textField.style.position = 'absolute'
    textField.style.left = '-9999px'
    textField.readOnly = true // avoid iOs keyboard opening
    textField.contentEditable = 'true'
    document.body.appendChild(textField)
    const isIOS = navigator.userAgent.match(/ipad|ipod|iphone/i)
    if (isIOS) {
      const range = document.createRange()
      range.selectNodeContents(textField)
      const selection = window.getSelection() // current text selection
      selection.removeAllRanges()
      selection.addRange(range)
      textField.setSelectionRange(0, bankAccount.length)
    } else {
      textField.select()
    }
    document.execCommand('copy')
    textField.remove()
    intervalRef = setTimeout(() => {
      setIsOpen(false)
    }, 2500)
  }

  const handleClose = () => {
    setIsOpen(false)
    clearTimeout(intervalRef)
  }

  return (
    <Formik
      validationSchema={ProfileSchema}
      initialValues={{
        userName: userName,
        amount: NaN,
        transferBy: '',
      }}
      onSubmit={(values, { setSubmitting }) => {
        if (values.amount >= process.env.REACT_APP_MIN_DEPO) {
          api
            .depositMoney(userName, values.amount, values.transferBy, imageFile)
            .then(x => x.json())
            .then(updatedInfo => {
              if (updatedInfo.err) {
                ctx.setErr(true, updatedInfo.err_message, false, 'Error')
                setSubmitting(false)
                return
              }
              setSubmitting(false)
              ctx.setErr(
                true,
                `Deposit ${values.amount}!, ${translate(ctx.preferredLocale, 'profile.depositSuccessNotif')}`,
              )
              values.amount = 0
            })
            .catch(e => console.error(e))
        } else {
          setSubmitting(false)
          alert('...Min deposit 100 Tb, Invalid amount')
        }
      }}>
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        setFieldValue,

        /* and other goodies */
      }) => (
        <Form loading={isSubmitting || uploading} onSubmit={handleSubmit} error={errors}>
          <Form.Group widths='equal'>
            <Form.Field
              value={userName}
              name='formAccount'
              onChange={handleChange}
              disabled
              onBlur={handleBlur}
              required
              label='From Account'
              control='input'
            />
          </Form.Group>
          <List divided verticalAlign='middle'>
            {banks &&
              banks.length > 0 &&
              banks.map(bank => {
                if (_.includes(bank.bankName, userBank.bank)) {
                  return (
                    <List.Item as={Segment} style={{ padding: 16 }}>
                      <List.Content floated='right'>
                        <Popup
                          trigger={bank.bankStatus ? <Button>Copy</Button> : ''}
                          on='click'
                          content={'Copied'}
                          onOpen={e => {
                            e.preventDefault()
                            handleOpen(bank.bankAccountNo)
                          }}
                          onClose={() => handleClose()}
                          position='top right'
                        />
                      </List.Content>
                      <Image avatar src={bank.image} style={bank.bankStatus ? {} : { filter: 'grayscale(100%)' }} />
                      <List.Content>
                        <Translate string={`bank.${bank.bankName}`} />
                        <div style={{ color: 'black', fontWeight: 'bold' }}>{bank.bankAccountName}</div>
                        <div style={{ color: 'black' }}>{bank.bankStatus ? bank.bankAccountNo : 'OFFLINE'}</div>
                      </List.Content>
                    </List.Item>
                  )
                }
                if (bank.isShow) {
                  return (
                    <List.Item as={Segment} style={{ padding: 16 }}>
                      <List.Content floated='right'>
                        <Popup
                          trigger={<Button id={bank.bankAccount}>Copy</Button>}
                          on='click'
                          content={'Copied'}
                          onOpen={e => {
                            e.preventDefault()
                            handleOpen(bank.bankAccountNo)
                          }}
                          onClose={() => handleClose()}
                          position='top right'
                        />
                      </List.Content>
                      <Image avatar src={bank.image} style={bank.bankStatus ? {} : { filter: 'grayscale(100%)' }} />

                      <List.Content>
                        <div style={{ color: 'black', fontWeight: 'bold' }}>
                          <Translate string={`bank.${bank.bankName}`} />
                        </div>
                        <div style={{ color: 'black', fontWeight: 'bold' }}>{bank.bankAccountName}</div>
                        <div style={{ color: 'black' }}>{bank.bankAccountNo}</div>
                      </List.Content>
                    </List.Item>
                  )
                }
              })}
          </List>
          <Form.Group inline widths='equal'>
            <label>Channel</label>
            <Form.Radio
              name='transferBy'
              label='Bank'
              value='Bank'
              checked={values.transferBy === 'Bank'}
              // onChange={handleChange}
              onChange={(e, { name, value }) => {
                setFieldValue(name, value)
              }}
            />
            <Form.Radio
              name='transferBy'
              label='Pulsa Telkomsel'
              value='pulsatsel'
              checked={values.transferBy === 'pulsatsel'}
              // onChange={handleChange}
              onChange={(e, { name, value }) => {
                setFieldValue(name, value)
              }}
            />
            <Form.Radio
              name='transferBy'
              label='Pulsa XL'
              value='pulsaxl'
              checked={values.transferBy === 'pulsaxl'}
              // onChange={handleChange}
              onChange={(e, { name, value }) => {
                values.amount = 0
                setFieldValue(name, value)
              }}
            />
            <Form.Radio
              name='transferBy'
              label='OVO / GoPay'
              value='ovoGoPay'
              checked={values.transferBy === 'ovoGoPay'}
              // onChange={handleChange}
              onChange={(e, { name, value }) => {
                setFieldValue(name, value)
              }}
            />
          </Form.Group>
          {touched.transferBy && errors.transferBy && <span className={'formError'}> {errors.transferBy}</span>}
          <Grid columns='equal' stackable>
            <Grid.Column width='8'>
              <Form.Field
                value={values.amount}
                placeholder='1 = 1000 Rupiah'
                name='amount'
                onChange={handleChange}
                // onBlur={handleBlur}
                onBlur={e => {
                  e.preventDefault()
                  if (values.transferBy === 'pulsatsel') values.amount = values.amount * 0.8
                  if (values.transferBy === 'pulsaxl') values.amount = values.amount * 0.9
                }}
                required
                label={
                  <label>
                    <Translate string='profile.amount' />
                  </label>
                }
                control='input'
                type='number'
                error={_.has(errors, 'amount') && _.has(touched, 'amount')}
              />
              {touched.amount && errors.amount && <span className={'formError'}> {errors.amount}</span>}
            </Grid.Column>
            <Grid.Column>
              <span style={{ color: 'black', margin: '-15px 0 0 10px', padding: '0 0 20px 0' }}>
                Rupiah :{' '}
                {/* {(() => {
                    if (values.transferBy === 'pulsatsel')
                      return new Intl.NumberFormat('en-ID', { maximumSignificantDigits: 2 }).format(
                        values.amount * 1000 * 0.8,
                      )
                    if (values.transferBy === 'pulsaxl')
                      return new Intl.NumberFormat('en-ID', { maximumSignificantDigits: 2 }).format(
                        values.amount * 1000 * 0.9,
                      )
                    return new Intl.NumberFormat('en-ID', { maximumSignificantDigits: 2 }).format(values.amount * 1000)
                  })()} */}
                {new Intl.NumberFormat('en-ID', { maximumSignificantDigits: 2 }).format(values.amount * 1000)}
              </span>
            </Grid.Column>
          </Grid>
          <p></p>
          <Form.Group widths='equal'>
            <Form.Field>
              <input
                onChange={onFileUpload}
                ref={fileInputRef}
                hidden
                accept='.png, .jpg, .jpeg'
                id='upload'
                type='file'
              />
              <Button
                content={<Translate string={'profile.fileupload'} />}
                labelPosition='left'
                icon='file'
                type={'button'}
                onClick={() => {
                  fileInputRef.current.click()
                }}
              />
            </Form.Field>
          </Form.Group>
          <div style={{ paddingBottom: 16 }}>
            {imagePreviewUrl ? <Image src={imagePreviewUrl} size='medium'></Image> : null}
          </div>

          <Button
            type='submit'
            onClick={handleSubmit}
            style={{ backgroundColor: '#0b8040', color: 'white' }}
            fluid
            size='large'>
            <Translate string={'profile.deposit'} />
          </Button>
        </Form>
      )}
    </Formik>
  )
}

export default DepositForm
