import React, { useContext, lazy, Suspense } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import ChangePasswordScreen from './screens/ChangePassword'
import ProfileScreen from './screens/ProfileScreen'
import PBSScreen from './screens/PBSScreen'
import { UserContext } from './contexts/User'
import ForgetPassword from './screens/ForgetPassword'

const HomeScreen = lazy(() => import('./screens/HomeScreen/HomeScreen'))
const HomeFC = lazy(() => import('./screens/HomeScreen/HomeFC'))
const LoginScreen = lazy(() => import('./screens/LoginScreen'))
const RegisterScreen = lazy(() => import('./screens/RegisterScreen'))
const PromoScreen = lazy(() => import('./screens/PromoScreen'))

const renderLoader = () => (
  <div class='wrap'>
    <div class='centerpage loading'>
      <div class='pills'></div>
      <div class='paco'>
        <div class='head'></div>
      </div>
      <div class='text-loader'>
        <p>FUN & JOY, Remember FASTGAMES89!!!</p>
      </div>
    </div>
  </div>
)

const Navigator = () => {
  const ctx = useContext(UserContext)
  return (
    <Suspense fallback={renderLoader()}>
      <Switch>
        {ctx.user && (ctx.user.isFC || ctx.user.isPromo) ? (
          <Route exact path='/' component={HomeFC} />
        ) : (
          <Route exact path='/' render={props => <HomeScreen {...props} />} />
        )}
        <Route exact path='/' render={props => <HomeScreen {...props} />} />
        <Route exact path='/promo' render={props => <PromoScreen {...props} />} />
        {ctx.user ? (
          <Switch>
            <Route exact path='/changePassword' component={ChangePasswordScreen} />
            <Route exact path='/profile' component={ProfileScreen}></Route>
            <Route exact path='/statement' component={PBSScreen}></Route>

            <Redirect to='/login' />
          </Switch>
        ) : (
          <Switch>
            <Route exact path='/forgetPassword' component={ForgetPassword}></Route>
            <Route exact path='/login' render={props => <LoginScreen {...props} />}></Route>

            <Route exact path='/register' component={RegisterScreen} />
            <Redirect to='/login' />
          </Switch>
        )}
      </Switch>
    </Suspense>
  )
}

export default Navigator
